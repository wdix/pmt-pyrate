#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time

from pyrate.jobs import job_WD_avg_waveform, job_WD_SPE_laser, job_WD_SPE_PMT, job_WD_darkrate, job_WaveDump ,job_WaveCatcher, job_ROOTHists, job_ROOTree, job_CREDO

"""
Load the list of jobs to be run.
"""

job_list = []
job_list += [job_WD_avg_waveform.job]
#job_list += [job_WD_OldRuns.job]    
#job_list += [job_WaveDump.job]    
#job_list += [job_ROOTHists.job] 
#job_list += [job_ROOTree.job] 
#job_list += [job_WaveCatcher.job] 
#job_list += [job_CREDO.job]
#job_list += [job_WD_darkrate.job]
#job_list += [job_WD_SPE_laser.job]
#job_list += [job_WD_SPE_PMT.job]
#job_list += [job_WD_darkrate.job]

if __name__ == "__main__":
  for job in job_list: 
    job.launch()

# EOF
