#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time

from pyrate.jobs import job_WD_darkrate_func_temp, job_WD_darkrate

"""
Load the list of jobs to be run.
"""

job_list = []
#job_list += [job_WD_darkrate.job]
job_list += [job_WD_darkrate_func_temp.job]

if __name__ == "__main__":
  for job in job_list: 
    job.launch()

# EOF
