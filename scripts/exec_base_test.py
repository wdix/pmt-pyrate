#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time

from pyrate.jobs import job_WD_DR_PMT_Basetest, job_WD_SPE_PMT_Basetest
"""
Load the list of jobs to be run.
"""

job_list = []
#job_list += [job_WD_SPE_PMT_Basetest.job]
job_list += [job_WD_DR_PMT_Basetest.job]


if __name__ == "__main__":
  for job in job_list: 
    job.launch()

# EOF
