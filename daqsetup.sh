## pre-setup, don't touch

path_of_this_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export MAIN=${path_of_this_dir}

add_to_python_path()
{
    export PYTHONPATH=$1:$PYTHONPATH
    echo "  Added $1 to your PYTHONPATH."
}

add_to_path()
{
    export PATH=$1:$PATH
    echo "  Added $1 to your PATH."
}

# ----------------
# setup PYTHONPATH
# ----------------

echo "  Setting up your PYTHONPATH."
add_to_python_path ${MAIN}
#add_to_python_path ${MAIN}/pyrate
echo "  done."

# ---------------------------------------------
# Add sabre_muons_daq/scripts directory to PATH
# ---------------------------------------------
#echo "  Add scripts to PATH."
#add_to_path ${MAIN}/scripts
#echo "  done."


export DAQ_SCRIPTS=$(pwd)/scripts
if [ -n "${PATH}" ]; then
    export PATH=${DAQ_SCRIPTS}:${PATH}
else
    export PATH=${DAQ_SCRIPTS}
fi

#eval "setupROOT6.14.04"

#echo "  Setting up numba... coepp specific"
#echo "  module load sl6/gcc484/python274/coepp/pytools-1.0.0.x86_64"
#echo "  module load sl6/gcc484/python-2.7.14.x86_64"
#echo "  module load sl6/gcc484/llvm-5.0.1.x86_64"

#module load sl6/gcc484/python274/coepp/pytools-1.0.0.x86_64
#module load sl6/gcc484/python-2.7.14.x86_64
#module load sl6/gcc484/llvm-5.0.1.x86_64

pip install --user file_read_backwards
pip install --user scipy

setupATLAS
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"

#eval "setupROOT6.14.04"





