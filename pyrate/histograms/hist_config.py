#!/usr/bin/env python
# -*- coding: utf-8 -*-

description="""
This file holds the configuration of all histograms.
The vname string should correspond to the name of a
variable in the store used to fill the histograms.
"""
from pyrate.classes import Hist1D

test_hist = Hist1D(name   = "test_hist",
                 nbins  = 100,
                 xmin   = 0,
                 xmax   = 100,
                 vname  = "evnumber",
                 xtitle = "Random Variable",
                 ytitle = "Entries",
                )

wmax_ch0_hist = Hist1D(name   = "waveform_max",
                 nbins  = 1000,
                 xmin   = -1,
                 xmax   = 1,
                 vname  = "wmax_ch0",
                 xtitle = "Waveform maximum [CH0]",
                 ytitle = "Entries",
                )

unixtime_hist = Hist1D(name   = "unixtime",
                 nbins  = 1000,
                 xmin   = 1554948550.000,
                 xmax   = 1554948551.000,
                 vname  = "unixtime",
                 xtitle = "UnixTime [s]",
                 ytitle = "Entries",
                )

tdc_corr_hist = Hist1D(name   = "tdc_corrected",
                 nbins  = 100,
                 xmin   = 0,
                 xmax   = 100,
                 vname  = "tdc_corrected",
                 xtitle = "TDC corrected",
                 ytitle = "Entries",
                )

event_number_hist = Hist1D(name   = "event_number",
                 nbins  = 80000,
                 xmin   = 0,
                 xmax   = 8000000,
                 vname  = "eventnumber",
                 xtitle = "Event number",
                 ytitle = "Entries",
                )


#List of histograms for PMT analysis and reconstruciton
##################################################################

#Laser Peak Histogram
#-----------------------------------------------------------------
laser_peak_location_hist = Hist1D(name   = "laser_peak_location",
                 nbins  = 2000,
                 xmin   = 0,
                 xmax   = 2000,
                 vname  = "laser_peak_location",
                 xtitle = "Laser Peak Location [samples]",
                 ytitle = "Entries",
                )


#SPE Histograms
#-------------------------------------------------------------------
spe_signal_charge_hist = Hist1D(name   = "spe_signal_charge",
                 nbins  = 10000,
                 xmin   = 0,
                 xmax   = 1000,
                 vname  = "signal_charge",
                 xtitle = "SPE Charge [pC]",
                 ytitle = "Entries",
                )

spe_sidebar_charge_hist = Hist1D(name   = "spe_sidebar_charge",
                 nbins  = 10000,
                 xmin   = 0,
                 xmax   = 1000,
                 vname  = "sidebar_charge",
                 xtitle = "SPE Charge [pC]",
                 ytitle = "Entries",
                )


#Ch0 Histograms
# -----------------------------------------------------------------------				
adcmax_ch0_hist = Hist1D(name   = "adcmax_ch0",
                 nbins  = 3200,
                 xmin   = 0,
                 xmax   = 16000,
                 vname  = "peak_height0",
                 xtitle = "Maximum ADC (above baseline) - Ch0",
                 ytitle = "Entries",
                )

peak_location_ch0_hist = Hist1D(name   = "adcmax_ch0",
                 nbins  = 3200,
                 xmin   = 0,
                 xmax   = 16000,
                 vname  = "peak_location0",
                 xtitle = "Maximum ADC (above baseline) - Ch0",
                 ytitle = "Entries",
                )                
				
charge_ch0_hist = Hist1D(name   = "ch0_charge",
                 nbins  = 10000,
                 xmin   = 0,
                 xmax   = 1000,
                 vname  = "ch0charge",
                 xtitle = "Integrated Charge [pC] - Ch0",
                 ytitle = "Entries",
                )
				
psd_ch0_hist = Hist1D(name   = "ch0_psd",
                 nbins  = 100,
                 xmin   = 0,
                 xmax   = 1,
                 vname  = "ch0psd",
                 xtitle = "Pulse Shape Variable - Ch0",
                 ytitle = "Entries",
                )	



# EOF

