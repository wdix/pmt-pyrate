from pyrate.classes import Algorithm
import numpy as np
from scipy.fftpack import rfft, fftfreq
import matplotlib.pyplot as plt
import pmt_constants as CONSTANTS
import os
import sys
import ROOT
from ROOT import TH1F, TH1D, TH2D, TFile, TGraphErrors, TCanvas, TTree
from ROOT import kRed, kBlue, kBlack

#####################################################################
#   Root Dark Rate Histogram
#####################################################################
#   DEPENDENT ON PMT CALC VARS, Dark Rate Algorithm
#   The following algorithm generates four histograms from the dark rate data, the first is simply the peak height of the dark rate pulse,
#   the second is a cumulative histogram corrected as a rate
#
class ROOT_Dark_Rate_Histogram(Algorithm):
    
    def __init__(self, name = "Dark_Rate_Histogram", out_path = "/home/wdix/Documents/PMT_Analysis"):
        self.name=name
        self.out_path = out_path

    
    def initialize(self):
        if self.run.form == "wc":
            print(" ")
            print("Data collected from wavecatcher is currently not supported by the " +str(self.name) + " algorithm.")
            print(" ")
            pass
        
        s_factor = self.run.channels[0].plot_scale_factor
        n_bins = 200
        low_range = 0
        high_range = 1000
        low_charge = 0
        high_charge= 20
        self.pr_store["dr_peak_hist"] = TH1D("dr_peak_hist"+str(self.run.name),"Dark Rate Peak Height: " + str(self.run.name), int(n_bins*s_factor), low_range, high_range*s_factor)
        self.pr_store["dr_rate_hist"] = TH1D("dr_rate_hist"+str(self.run.name),"Dark Rate as a Function of Peak Height: " +str(self.run.name), int(n_bins*s_factor), low_range, high_range*s_factor)
        self.pr_store["dr_charge_hist"] = TH1D("dr_charge_hist"+str(self.run.name),"Dark Rate Charge: " + str(self.run.name), int(n_bins*s_factor), low_charge, high_charge*s_factor)
        self.pr_store["dr_2d_hist"] = TH2D("dr_2d_hist"+str(self.run.name), "Dark Rate Charge vs. Peak Height: " +str(self.run.name), int(100*s_factor), 0, high_charge*s_factor, int(100*s_factor), 0, 1000*s_factor)
        
        pass

  
    def execute(self):
        if self.run.form == "wc":
            pass
        
        if self.run.form == "wd":
            #if self.tr_store["is_dark_rate"] == 1:
            peak_height = self.tr_store["peak_height0"]
            peak_location = self.tr_store["peak_location0"]
            pmt_ch = self.run.channels[0]
            pmt_waveform = pmt_ch.event.waveform
            temp_charge = 0

            if((CONSTANTS.PREPEAK_CHARGE_WINDOW + CONSTANTS.NUM_BASELINE) < peak_location ) and (peak_location < (pmt_ch.event.record_length- (CONSTANTS.CHARGE_WINDOW_WIDTH -CONSTANTS.CHARGE_WINDOW_START))):
                charge_window_start = peak_location - CONSTANTS.PREPEAK_CHARGE_WINDOW
                for i in range(charge_window_start, charge_window_start + CONSTANTS.CHARGE_WINDOW_WIDTH):
                    temp_charge += self.tr_store["baseline0"] - pmt_waveform[i]

            pulse_charge = temp_charge *CONSTANTS.CHARGE_CONSTANT
            self.pr_store["dr_peak_hist"].Fill(peak_height)
            self.pr_store["dr_charge_hist"].Fill(pulse_charge)
            self.pr_store["dr_2d_hist"].Fill(pulse_charge, peak_height)
            
            #fill variables in store used to write out chain
            self.tr_store["peak_height_pmt"] = peak_height
            self.tr_store["peak_location_pmt"] = peak_location
            self.tr_store["baseline_pmt"] = self.tr_store["baseline0"]
            self.tr_store["charge_pmt"] = pulse_charge


    def finalize(self):
        # This fills the cumulative rate histogram with all events in higher bins in the peak heigh histogram
        n_bins = self.pr_store["dr_peak_hist"].GetNbinsX()
        run_time = self.pr_store["run_time_corrected"]
        for i in range (0, n_bins):
            temp_cumulative = 0
            for idx in range (i, n_bins):
                temp_cumulative += self.pr_store["dr_peak_hist"].GetBinContent(idx)
            
            rate = 1.00*temp_cumulative/run_time
            self.pr_store["dr_rate_hist"].SetBinContent(i, rate)
        
        canvas_1 = TCanvas("PMT peak_height","PMT pulse",1200,800)
        canvas_1.SetLogy()
        self.pr_store["dr_peak_hist"].GetXaxis().SetTitle("Peak Height [ADC Value - Baseline]")
        self.pr_store["dr_peak_hist"].GetYaxis().SetTitle("Count")
        self.pr_store["dr_peak_hist"].Draw()
        canvas_1.SaveAs(os.path.join(self.out_path, (self.run.name + "-peak.pdf" )))
        
        canvas_2 = TCanvas("PMT peak_height","PMT pulse",1200,800)
        canvas_2.SetLogy()
        self.pr_store["dr_rate_hist"].GetXaxis().SetTitle("Peak Height [ADC Value - Baseline]")
        self.pr_store["dr_rate_hist"].GetYaxis().SetTitle("Dark Rate [Hz]")
        self.pr_store["dr_rate_hist"].Draw()
        canvas_2.SaveAs(os.path.join(self.out_path, (self.run.name + "-rate.pdf" )))
        
        canvas_3 = TCanvas("PMT pulse charge","PMT pulse",1200,800)
        self.pr_store["dr_charge_hist"].GetXaxis().SetTitle("Pulse Charge [pC]")
        self.pr_store["dr_charge_hist"].GetYaxis().SetTitle("Counts")
        self.pr_store["dr_charge_hist"].Draw()
        canvas_3.SaveAs(os.path.join(self.out_path, (self.run.name + "-charge.pdf" )))
        
        canvas_4 = TCanvas("PMT 2d Histogram","PMT pulse",1200,800)
        self.pr_store["dr_2d_hist"].GetXaxis().SetTitle("Pulse Charge [pC]")
        self.pr_store["dr_2d_hist"].GetYaxis().SetTitle("Peak Height [ADC Value - Baseline]")
        self.pr_store["dr_2d_hist"].Draw("COLZ")
        canvas_4.SaveAs(os.path.join(self.out_path, (self.run.name + "-charge_height.pdf" )))


#####################################################################
# ROOT Find Laser Peak
#####################################################################
#   The following algorithm determines and generates the plot of laser signal peak location
#       to define the trigger window. It does not require other algorithms.
#       It only records events where the peak exceeds the laser threshold.
#
#   Variables created in the permanant/transient store
#       "laser_location_array"  pr_store    Array of laser location data used to produce histograms
#       "laser_peak_height"     tr_store    Laser peak height in absolute adc. if = -1 then pulse did not exceed threshold
#       "peak_location_laser"   tr_store    Laser peak location. if = -1 then pulse did not exceed threshold
###

class ROOT_Find_Laser_Peak(Algorithm):
    def __init__(self, name = "ROOT_Find_Laser_Peak", out_path = "/home/wdix/Documents/PMT_Analysis"):
        self.name = name
        self.out_path = out_path
        pass
    

    def initialize(self):
        num_samples = 520
        self.pr_store["laser_location_array"]=[]
        self.pr_store["laser_location_hist"]= TH1D("laser_location_hist"+str(self.run.name),"Laser Peak Location: " + str(self.run.name), num_samples, 0, num_samples)
        if self.run.form == "wc":
            print(" ")
            print("Data collected from wavecatcher is currently not supported by the " +str(self.name) + " algorithm.")
            print(" ")
        pass
  

    def execute(self):
        if self.run.form == "wc":
            pass
        
        if self.run.form == "wd":
            for ch in self.run.channels:
                if ch.source_type in {"LASER","Laser"}:
                    laser_waveform = ch.event.waveform
                    self.tr_store["laser_peak_height"] = -1
                    self.tr_store["peak_location_laser"] = -1

                    #Peak finding loop
                    height_temp = 0
                    location_temp = 0
                    for i in range (0,ch.event.record_length):
                        if laser_waveform[i] > height_temp:
                            height_temp = laser_waveform[i]
                            location_temp = i
                    
                    # Check if peak exceeds laser threshold
                    if height_temp > CONSTANTS.LASER_THRESHOLD:
                        self.tr_store["laser_peak_height"] = height_temp
                        self.tr_store["peak_location_laser"] = location_temp
                        self.pr_store["laser_location_array"].append(location_temp)
                        self.pr_store["laser_location_hist"].Fill(location_temp)
 

    def finalize(self):
        root_file = TFile( os.path.join(self.out_path, self.run.name +"laser_peak_hist.root"), 'RECREATE', 'Root file which stores histograms generated by ' +self.name)
        canvas_1 = TCanvas("Laser Peak Location","PMT pulse",1200,800)
        canvas_1.SetLogy()
        self.pr_store["laser_location_hist"].GetXaxis().SetTitle("Laser Peak Time [2ns Samples]")
        self.pr_store["laser_location_hist"].GetYaxis().SetTitle("Count")
        self.pr_store["laser_location_hist"].Draw()
        canvas_1.SaveAs(os.path.join(self.out_path, (self.run.name + "-laser_location.pdf" )))
        root_file.Write()
        pass



#####################################################################
#   ROOT SPE (Single PhotoElectron) Charecterisation Algorithm
#       REQUIRES PMT CALC VARS TO BE RUN FIRST
#####################################################################
#   The following algorithm is used in single photoelectron characterisation measurements
#       it checks for a laser signal in a suitable time window (with a sideband region also identified), 
#       recalculates the pulse charge and and expresses the variables in a consistant manner for root histograms.
#       Pulse charge is either fixed or floating but window must not overlap with baseline calculation
#
#   Individual event is classified as (stored in self.tr_store["signal_type"])
#       0 - non signal event
#       1 - signal event
#       2 - sidebar event
#
#   The Algorithm has several optional arguments (default values are defined in pmt_constants.py)
#       signal_region_start     - start location of the signal acceptance window (in samples)
#       signal_region_width     - width of signal and sidebar region (insamples)
#       sidebar_region_delay    - distance between end of signal region and start of sidebar region
#       charge_window_width     - width of window used to determine SPE pulse charge
#       prepeak_charge_window   - number of samples of charge window taken before the waveform peak if window is allowed to move
#       charge_window_start     - gives start of charge window if fixed window location is used (-1 if not used, which is default)
#
#   Variables created in the permanant/transient store 
#       "laser_ch_num"      pr_store    Channel number of laser channel
#       "pmt_ch_num"        pr_store    Channel number of pmt channel
#       "num_signal"        pr_store    Number of signal events
#       "num_sidebar"       pr_store    Number of sidebar events
#       "window_error"      pr_store    Flag for error in hardcoded window error (1 = error)
#       "signal_type"       tr_store    event classification
#       "signal_charge"     tr_store    charge of signal events (not used for non-signal events)
#       "sidebar_charge"    tr_store    charge of sidebar events (not used for non-sidebar events)
#
class ROOT_SPE_Characterisation(Algorithm):
    
    def __init__(self, name = "ROOT_SPE_Characterisation", out_path = "/home/wdix/Documents/PMT_Analysis", signal_region_start = CONSTANTS.SIGNAL_REGION_START, signal_region_width = CONSTANTS.SIGNAL_REGION_WIDTH, sidebar_region_delay = CONSTANTS.SIDEBAR_REGION_DELAY, charge_window_width = CONSTANTS.CHARGE_WINDOW_WIDTH, prepeak_charge_window = CONSTANTS.PREPEAK_CHARGE_WINDOW, charge_window_start = -1):
        self.name=name
        self.out_path = out_path
        self.signal_region_start = signal_region_start
        self.signal_region_width = signal_region_width
        self.sidebar_region_delay  = sidebar_region_delay
        self.charge_window_width = charge_window_width
        self.prepeak_charge_window = prepeak_charge_window
        self.charge_window_start = charge_window_start
        pass
    

    def initialize(self):
        if self.run.form == "wc":
            print(" ")
            print("Data collected from wavecatcher is currently not supported by the " +str(self.name) + " algorithm.")
            print(" ")

        if self.run.form == "wd":
            #Declare histograms
            n_bins = 200
            low_range = 0
            high_range = 1000
            low_charge = 0
            high_charge= 20
            s_factor = self.run.channels[0].plot_scale_factor
            self.pr_store["spe_peak_hist_sg"]= TH1D("spe_peak_hist_sg"+str(self.run.name),"SPE Peak Height: " + str(self.run.name), int(n_bins*s_factor), low_range, high_range*s_factor)
            self.pr_store["spe_peak_hist_bg"]= TH1D("spe_peak_hist_bg"+str(self.run.name),"SPE Peak Height: " + str(self.run.name), int(n_bins*s_factor), low_range, high_range*s_factor)
            self.pr_store["spe_charge_hist_sg"] = TH1D("spe_charge_hist_sg"+str(self.run.name),"SPE Charge: " + str(self.run.name), int(n_bins*s_factor), low_charge, high_charge*s_factor)
            self.pr_store["spe_charge_hist_bg"] = TH1D("spe_charge_hist_bg"+str(self.run.name),"SPE Charge: " + str(self.run.name), int(n_bins*s_factor), low_charge, high_charge*s_factor)
            self.pr_store["spe_2d_hist_sg"] = TH2D("spe_2d_hist_sg"+str(self.run.name), "SPE Charge vs. Peak Height: " +str(self.run.name), int(100*s_factor), 0, high_charge*s_factor, int(100*s_factor), 0, 1000*s_factor)
            self.pr_store["spe_2d_hist_bg"] = TH2D("spe_2d_hist_bg"+str(self.run.name), "SPE Charge vs. Peak Height: " +str(self.run.name), int(100*s_factor), 0, high_charge*s_factor, int(100*s_factor), 0, 1000*s_factor)
            
            #determine which channel is laser / PMT
            ch_num = 0
            for ch in self.run.channels:
                if ch.source_type.upper() == "laser".upper():
                    self.pr_store["laser_ch_num"] = ch_num
                if ch.source_type.upper() == "PMT".upper():
                    self.pr_store["pmt_ch_num"] = ch_num
                ch_num += 1

            self.pr_store["num_signal"] = 0
            self.pr_store["num_sidebar"] =0

            #checks that hard coded windows are valid (no element will be out of range)
            event_length =  self.run.channels[0].event.record_length
            #Laser signal and sidebar window
            self.pr_store["window_error"] = 0
            if (self.signal_region_start<0):
                self.pr_store["window_error"] = 1
                print(" ")
                print("ERROR: Laser signal region is out of range for SPE characterisation - under")
                print(" ")
                pass
            if (self.signal_region_start+self.signal_region_width>=event_length):
                self.pr_store["window_error"] = 1
                print(" ")
                print("ERROR: Laser signal region is out of range for SPE characterisation - over")
                print(" ")
                pass
            if (self.signal_region_start + self.signal_region_width + self.sidebar_region_delay <0):
                self.pr_store["window_error"] = 1
                print(" ")
                print("ERROR: Laser sidebar region is out of range for SPE characterisation - under")
                print(" ")
                pass
            if (self.signal_region_start + self.signal_region_width + self.sidebar_region_delay + self.signal_region_width >=event_length):
                self.pr_store["window_error"] = 1
                print(" ")
                print("ERROR: Laser sidebar region is out of range for SPE characterisation - over")
                print(" ")
                pass
            #Check that hardcode charge window is valid (if used)
            if (self.charge_window_start>=0):
                if (self.charge_window_start <CONSTANTS.NUM_BASELINE):
                    self.pr_store["window_error"] = 1
                    print(" ")
                    print("ERROR: Pulse charge region is out of range for SPE characterisation - under")
                    print(" ")
                    pass
                if (self.charge_window_start + self.charge_window_width >=event_length):
                    self.pr_store["window_error"] = 1
                    print(" ")
                    print("ERROR: Pulse charge region is out of range for SPE characterisation - over")
                    print(" ")
                    pass

        pass
  
  
    def execute(self):
        if self.run.form.upper() == "wc".upper():
            pass
        
        if self.run.form.upper() == "wd".upper():
            # skips events if hardcoded window is not valid
            if (self.pr_store["window_error"]==1):
                print("WINDOW ERROR")
                pass

            # the following checks for laser in signal or sidebar region
            laser_ch = self.run.channels[self.pr_store["laser_ch_num"]]
            laser_waveform = laser_ch.event.waveform
            self.tr_store["peak_location_laser"] = self.tr_store["peak_location" + str(self.pr_store["laser_ch_num"])]
            #print("LASER PEAK location: "+ str(self.tr_store["peak_location_laser"]))
            #classify as signal or sidebar region
            self.tr_store["signal_type"] = 0
            if laser_waveform[self.tr_store["peak_location_laser"]]> CONSTANTS.LASER_THRESHOLD: #forces laser pulse to be above threshold
                if (self.signal_region_start <= self.tr_store["peak_location_laser"]) and (self.tr_store["peak_location_laser"] < (self.signal_region_start + self.signal_region_width)):
                    self.tr_store["signal_type"]=1
                    #print("EVENT is SIGNAL")
                sidebar_region_start = self.signal_region_start + self.signal_region_width + self.sidebar_region_delay
                if (sidebar_region_start <= self.tr_store["peak_location_laser"]) and (self.tr_store["peak_location_laser"] < (sidebar_region_start + self.signal_region_width)):
                    self.tr_store["signal_type"]=2
                    #print("EVENT is SIDEBAR")

            #calculate integrated charge for signal and sidebar events
            pmt_ch = self.run.channels[self.pr_store["pmt_ch_num"]]
            pmt_ch_num = self.pr_store["pmt_ch_num"]
            pmt_peak_height = self.tr_store["peak_height"+str(pmt_ch_num)]
            if self.tr_store["signal_type"]>=0:
                pmt_waveform = pmt_ch.event.waveform
                temp_charge = 0.000

                #hardcoded charge window
                if self.charge_window_start >= 0:
                    for i in range(self.charge_window_start, self.charge_window_start + self.charge_window_width):
                        temp_charge += self.tr_store["baseline" +str(self.pr_store["pmt_ch_num"])] - pmt_waveform[i]

                #peak centred charge window
                if self.charge_window_start < 0:
                    #check if window is valid
                    pmt_peak_location = self.tr_store["peak_location" + str(self.pr_store["pmt_ch_num"])]
                    
                    
                    if((self.prepeak_charge_window + CONSTANTS.NUM_BASELINE) < pmt_peak_location ) and (pmt_peak_location < (pmt_ch.event.record_length- (self.charge_window_width -self.prepeak_charge_window))):
                        charge_window_start = pmt_peak_location - self.prepeak_charge_window
                        for i in range(charge_window_start, charge_window_start + self.charge_window_width):
                            temp_charge += self.tr_store["baseline" +str(self.pr_store["pmt_ch_num"])] - pmt_waveform[i]
                    
                    else:
                        temp_charge = -1000
                    
            #fill elements of store used to write root trees
            self.tr_store["charge_pmt"] = temp_charge * CONSTANTS.CHARGE_CONSTANT
            self.tr_store["baseline_pmt"] = self.tr_store["baseline" +str(self.pr_store["pmt_ch_num"])]
            self.tr_store["peak_height_pmt"] = self.tr_store["peak_height"+str(self.pr_store["pmt_ch_num"])]
            self.tr_store["peak_location_pmt"] = pmt_peak_location
            self.tr_store["peak_height_laser"] = self.tr_store["peak_height" + str(self.pr_store["laser_ch_num"])]


            # express charge as either signal or sidebar for histogram
            if self.tr_store["signal_type"]==1: #is signal
                self.tr_store["signal_charge"] = temp_charge * CONSTANTS.CHARGE_CONSTANT
                
                self.pr_store["num_signal"] += 1
                #fill histograms
                self.pr_store["spe_peak_hist_sg"].Fill(pmt_peak_height)
                self.pr_store["spe_charge_hist_sg"].Fill(temp_charge * CONSTANTS.CHARGE_CONSTANT)
                self.pr_store["spe_2d_hist_sg"].Fill(temp_charge * CONSTANTS.CHARGE_CONSTANT, pmt_peak_height, 1.0)

            elif self.tr_store["signal_type"] == 2: #is sidebar
                self.tr_store["sidebar_charge"] = temp_charge * CONSTANTS.CHARGE_CONSTANT
                self.pr_store["num_sidebar"] += 1
                #Fill histograms
                self.pr_store["spe_peak_hist_bg"].Fill(pmt_peak_height)
                self.pr_store["spe_charge_hist_bg"].Fill(temp_charge * CONSTANTS.CHARGE_CONSTANT)
                self.pr_store["spe_2d_hist_bg"].Fill(temp_charge * CONSTANTS.CHARGE_CONSTANT, pmt_peak_height, 1.0)
        pass
 

    def finalize(self):
        run_time = (self.pr_store["run_end_time_corrected"] - self.pr_store["run_start_time"])*0.000000008
        if abs(run_time) > 0.02:
            signal_rate = 1.000*self.pr_store["num_signal"] / run_time
            sidebar_rate = 1.000*self.pr_store["num_sidebar"] / run_time
        else:
            signal_rate = 0
            sidebar_rate = 0

        print(" ")
        print("SPE Algorithm Output - Run: " + str(self.run.name))
        print("--------------------------------------------------------------")
        print("Signal Rate:  " + str(signal_rate) + str(" Hz"))
        print("Sidebar Rate: " + str(sidebar_rate) + str(" Hz"))
        print(" ")
        
        #Plot and save histograms
        out_root_file = TFile( os.path.join(self.out_path, self.run.name +"laser_peak_hist.root"), 'RECREATE', 'Root file which stores histograms generated by ' +self.name)
        out_root_file.cd()

        canvas_1 = TCanvas("SPE Peak Height","PMT pulse",1200,800)
        
        self.pr_store["spe_peak_hist_sg"].GetXaxis().SetTitle("Peak Height [adc-baseline]")
        self.pr_store["spe_peak_hist_sg"].GetYaxis().SetTitle("Count")
        self.pr_store["spe_peak_hist_sg"].Draw()
        canvas_1.Update()
        self.pr_store["spe_peak_hist_bg"].SetLineColor(kRed)
        self.pr_store["spe_peak_hist_bg"].Draw("same,HIST")

        canvas_1.SaveAs(os.path.join(self.out_path, (self.run.name + "-SPE_Peak_Height.pdf" )))
        canvas_1.SetLogy()
        canvas_1.Update()
        canvas_1.SaveAs(os.path.join(self.out_path, (self.run.name + "-SPE_Peak_Height_logy.pdf" )))
        
        canvas_2 = TCanvas("SPE Pulse Charge","PMT pulse",1200,800)
        self.pr_store["spe_charge_hist_sg"].GetXaxis().SetTitle("SPE Pulse Charge [pC]")
        self.pr_store["spe_charge_hist_sg"].GetYaxis().SetTitle("Count")
        self.pr_store["spe_charge_hist_sg"].Draw()
        canvas_2.Update()
        self.pr_store["spe_charge_hist_bg"].SetLineColor(kRed)
        self.pr_store["spe_charge_hist_bg"].Draw("same,HIST")
        canvas_2.SaveAs(os.path.join(self.out_path, (self.run.name + "-SPE_Pulse_Charge.pdf" )))
        

        canvas_3= TCanvas("SPE Pulse Charge vs Height - Signal","PMT pulse",1200,800)
        self.pr_store["spe_2d_hist_sg"].GetXaxis().SetTitle("SPE Pulse Charge [pC]")
        self.pr_store["spe_2d_hist_sg"].GetYaxis().SetTitle("Peak Height [adc-baseline]")
        self.pr_store["spe_2d_hist_sg"].Draw("COLZ")
        canvas_3.SaveAs(os.path.join(self.out_path, (self.run.name + "-SPE_Charge_vs_Peak_Height-Signal.pdf" )))

        canvas_4= TCanvas("SPE Pulse Charge vs Height - Background","PMT pulse",1200,800)
        self.pr_store["spe_2d_hist_bg"].GetXaxis().SetTitle("SPE Pulse Charge [pC]")
        self.pr_store["spe_2d_hist_bg"].GetYaxis().SetTitle("Peak Height [adc-baseline]")
        self.pr_store["spe_2d_hist_bg"].Draw("COLZ")
        canvas_4.SaveAs(os.path.join(self.out_path, (self.run.name + "-SPE_Charge_vs_Peak_Height-Background.pdf" )))
        
        out_root_file.Write()
        pass



#####################################################################
#   ROOT Pulse Shape Vars
#       REQUIRES PMT CALC VARS TO BE RUN FIRST
#####################################################################
#   The following algorithm is generates a range of pulse shape variables for PMT pulses.
#       Only one channel is used, if multiple channels are passed the algorithm finds 
#       the first PMT channel.
#
#   The Pulse shape variables currently generated are 
#       - 1st, 2nd, 3rd, and 4th moments
#       - amplitude weighted mean time
#       - pre peak charge/total charge
#       - tail charge / total charge
#       - mean pulse width (charge/peak height)
#       - time above threshold
#       - extended charge

#
#   The Algorithm has several optional arguments (default values are defined in pmt_constants.py)
#       pmt_channel             - channel to be analysed, if value is -1 the algorithm will find the first pmt channel

#       sidebar_region_delay    - distance between end of signal region and start of sidebar region
#       charge_window_width     - width of window used to determine SPE pulse charge
#       prepeak_charge_window   - number of samples of charge window taken before the waveform peak if window is allowed to move
#       charge_window_start     - gives start of charge window if fixed window location is used (-1 if not used, which is default)
#
#   Variables created in the permanant/transient store 
#       "window_error"      pr_store    Flag for error in hardcoded window error (1 = error) (same as generated by SPE algorithm)
#       "event_region_hist" tr_store    root histogram of the extended charge window used to calculate moments
#       "charge_extended"   tr_store    Calculated charge for the pulse with a wider window (defined in constants)
#       "charge_pre_peak"   tr_store    Charge in window up to (and including) the peak (using standard window)
#       "charge_post_peak"  tr_store    Charge in window from the peak to end (using standard window)
#       "charge_tail"       tr_store    Charge of 100 samples after inital charge window
#       "mean_time"         tr_store    Amplitude weighted mean time
#       "moment_1"          tr_store    Cacluated first moment of the pulse
#       "moment_2"          tr_store    Cacluated first moment of the pulse
#       "moment_3"          tr_store    Cacluated first moment of the pulse
#       "moment_4"          tr_store    Cacluated first moment of the pulse
#       "samples_above_threshold"   tr_store  number of samples above threshold

#
class ROOT_Pulse_Shape_Vars(Algorithm):
    def __init__(self, name = "ROOT_Pulse_Shape_Vars", pmt_channel = -1, charge_window_start = CONSTANTS.CHARGE_WINDOW_START, charge_window_width = CONSTANTS.CHARGE_WINDOW_WIDTH):
        self.name = name
        self.pmt_channel = pmt_channel
        self.charge_window_start = charge_window_start
        self.charge_window_width = charge_window_width

    
    def initialize(self):
        #find first pmt channel or if one is passed as optional argument confirm it is a pmt channel
        if self.pmt_channel >= 0:
            if self.run.channels[self.pmt_channel].source_type.upper() != "PMT".upper():
                print(" ")
                print("ERROR - Specified pmt channel is not from a pmt")
                print("     Run : " +str(self.run.name))
                print("     Specified channel: " +str(self.pmt_channel) + " is from a " +str(self.run.channels[self.pmt_channel].source_type))
                print(" ")
                self.pmt_channel = -1
        
        if self.pmt_channel == -1:
            ch_num = 0
            for ch in self.run.channels:
                if ch.source_type.upper() == "PMT".upper():
                    self.pmt_channel = ch_num
                    break
                ch_num += 1

        if self.pmt_channel == -1:
            print(" ")
            print("MAJOR ERROR")
            print("RUN " + str(self.run.name) + " contains no PMT channel.")
            print("Algorithm "+ str(self.name) + " will not run")
            print(" ") 

        #if hardcoded charge window is used, check if it is valid
        #checks that hard coded windows are valid (no element will be out of range)
        event_length =  self.run.channels[0].event.record_length
            
        #Check that hardcode charge window is valid (if used)
        if (self.charge_window_start>=0):
            if (self.charge_window_start <CONSTANTS.NUM_BASELINE):
                self.pr_store["window_error"] = 1
                print(" ")
                print("ERROR: Pulse charge region is out of range for SPE characterisation - under")
                print(" ")
                pass
            if (self.charge_window_start + self.charge_window_width >=event_length):
                self.pr_store["window_error"] = 1
                print(" ")
                print("ERROR: Pulse charge region is out of range for SPE characterisation - over")
                print(" ")
                pass

    
    def execute(self):
        if self.run.form.upper() == "wd".upper():
            # skips events if hardcoded window is not valid
            if (self.pr_store["window_error"]==1):
                pass
            
            #  check if peak centred window is valid for extended window
            if self.charge_window_start < 0:
                pmt_peak_location = self.tr_store["peak_location" + str(self.pmt_channel)]
                extended_window_start = pmt_peak_location - CONSTANTS.EXTENDEND_CHARGE_WINDOW_PREPEAK
                if extended_window_start <= CONSTANTS.NUM_BASELINE:
                    pass
                if (extended_window_start + CONSTANTS.EXTENDEND_CHARGE_WINDOW_WIDTH) >= self.run.channels[0].event.record_length:
                    pass

            # create variables that can be calculated in filling the extended event histogram
            self.tr_store["event_region_hist"] = TH1D("event_region_hist"+str(self.run.name)+str(self.tr_store["event_nr"]),"Waveform", CONSTANTS.EXTENDEND_CHARGE_WINDOW_WIDTH , 0, CONSTANTS.EXTENDEND_CHARGE_WINDOW_WIDTH -1)
            #placeholder variables used in calculation
            temp_charge_extended    = 0
            temp_charge_prepeak     = 0
            temp_charge_postpeak    = 0
            temp_meantime           = 0
            temp_samples_above      = 0
            
            pmt_ch = self.run.channels[self.pmt_channel]
            pmt_waveform = pmt_ch.event.waveform
            pmt_baseline = self.tr_store["baseline"+str(self.pmt_channel)]

            # Check if window is valid
            if (extended_window_start + CONSTANTS.EXTENDEND_CHARGE_WINDOW_WIDTH) < self.run.channels[0].event.record_length:
                if extended_window_start <= 0:
                    extended_window_start = 0
                
                #loop over entries in window
                for i in range(CONSTANTS.EXTENDEND_CHARGE_WINDOW_WIDTH):
                    idx = i + extended_window_start
                    adc = pmt_baseline - pmt_waveform[idx]
                    
                    self.tr_store["event_region_hist"].SetBinContent(i, adc)

                    temp_charge_extended += adc
                    temp_meantime += adc * i

                    if i <= CONSTANTS.EXTENDEND_CHARGE_WINDOW_PREPEAK:
                        temp_charge_prepeak += adc

                    if i >= CONSTANTS.EXTENDEND_CHARGE_WINDOW_PREPEAK:
                        temp_charge_postpeak += adc

                    if adc >= CONSTANTS.DEFAULT_TRIGGER_THRESHOLD:
                        temp_samples_above +=1

                #Fill variables in transient store
                self.tr_store["charge_extended"]    = temp_charge_extended  * CONSTANTS.CHARGE_CONSTANT
                self.tr_store["charge_pre_peak"]    = temp_charge_prepeak   * CONSTANTS.CHARGE_CONSTANT
                self.tr_store["charge_post_peak"]   = temp_charge_postpeak  * CONSTANTS.CHARGE_CONSTANT
                
                if temp_charge_extended != 0:
                    self.tr_store["mean_time"]      = temp_meantime / temp_charge_extended
                elif temp_charge_extended == 0:
                    self.tr_store["mean_time"]      = -100
  
            else:
                self.tr_store["charge_extended"]    = -100
                self.tr_store["charge_pre_peak"]    = -100
                self.tr_store["charge_post_peak"]   = -100
                self.tr_store["mean_time"]          = -100

            self.tr_store["samples_above_threshold"] = temp_samples_above
            self.tr_store["moment_1"] = self.tr_store["event_region_hist"].GetMean()
            self.tr_store["moment_2"] = (self.tr_store["event_region_hist"].GetStdDev())**2
            self.tr_store["moment_3"] = self.tr_store["event_region_hist"].GetSkewness()
            self.tr_store["moment_4"] = self.tr_store["event_region_hist"].GetKurtosis()

            #Calculate tail charge (returns -100 if tail is not fully defined)
            temp_charge_tail = 0
            charge_tail_start = extended_window_start + CONSTANTS.EXTENDEND_CHARGE_WINDOW_WIDTH
            #check tail is defined
            if (charge_tail_start  + CONSTANTS.CHARGE_TAIL_WIDTH) < self.run.channels[0].event.record_length:
                for i in range(0, CONSTANTS.CHARGE_TAIL_WIDTH):
                    idx = i + charge_tail_start
                    temp_charge_tail += (pmt_baseline - pmt_waveform[idx])
                self.tr_store["charge_tail"] = temp_charge_tail * CONSTANTS.CHARGE_CONSTANT
            
            else:
                self.tr_store["charge_tail"] = -100


    def finalize(self):
        pass



#####################################################################
#   ROOT  Average Waveform
#
#####################################################################
#   The following algorithm creates an average waveform (original and corrected adc)
#       for each channel. Also creates a cumulative version (only corrected)
#       The algorithm accepts a range of arguments to change the output
#   
#   Inputs:
#       normalise       - Flag if final waveform should be normalised
#       peak_centred    - Flag if avg waveform should be peak centred (if peak centred take 100 samples before and 300 after)
#           before_peak - gives number of samples before peak to be used if peak centred
#           after_peak  - gives number of samples after peak to be used if peak centred
#       check_type      - Flag if algorithm should check for signal type (requires SPE algorithm)
#
#   NOTE: if using peak centred average waveform and there are not sufficient samples before/after any out of range values 
#       will be filled as 0 (for corrected adc) or baseline (for original) 
#       
class ROOT_Average_Waveform(Algorithm):

    def __init__(self,  name = "ROOT_Average_Waveform", outpath = "~/Documents/",   normalise = True, peak_centred = True, check_type = False, before_peak = CONSTANTS.AVG_BEFORE_PEAK, after_peak = CONSTANTS.AVG_AFTER_PEAK):
        self.name           = name
        self.normalise      = normalise
        self.peak_centred   = peak_centred
        self.before_peak    = before_peak
        self.after_peak     = after_peak
        self.check_type     = check_type
        self.outpath        = outpath


    def initialize(self):
        # For peak centred waveforms load event length from either constants or passed values
        if self.peak_centred:
            self.array_size = self.before_peak + self.after_peak
        
        # Otherwise just take event length
        else:
            self.array_size = self.run.channels[0].record_length

        # Define histograms
        self.pr_store["avg_waveform"]               = TH1D("avg_waveform"+str(self.run.name), "Average Waveform - " + str(self.run.name), self.array_size, 0, self.array_size)
        self.pr_store["avg_waveform_uncorrected"]   = TH1D("avg_waveform_uncorr"+str(self.run.name), "Average Waveform Uncorrected - " + str(self.run.name), self.array_size, 0, self.array_size)
        self.pr_store["avg_waveform_cumulative"]    = TH1D("avg_waveform_cumul"+str(self.run.name), "Average Waveform Cumulative - " + str(self.run.name), self.array_size, 0, self.array_size)
        self.pr_store["num_avg_entries"]            = 0

        if self.check_type:  #Make background arrays if checking signal type
            self.pr_store["avg_waveform_bkgrnd"]               = TH1D("avg_bckgrnd_wave_hist"+str(self.run.name), "Average Background Waveform - " + str(self.run.name), self.array_size, 0, self.array_size)
            self.pr_store["avg_waveform_uncorrected_bkgrnd"]   = TH1D("avg_bckgrnd_wave_uncorr_hist"+str(self.run.name), "Average Background \ Waveform Uncorrected - " + str(self.run.name), self.array_size, 0, self.array_size)
            self.pr_store["avg_waveform_cumulative_bkgrnd"]    = TH1D("avg_bckgrnd_wave_cumul_hist"+str(self.run.name), "Average Background Waveform Cumulative - " + str(self.run.name), self.array_size, 0, self.array_size)
            self.pr_store["num_avg_entries_bkgrnd"]            = 0

        # Find first PMT channel 
        ch_num = 0
        self.pmt_channel = -1
        for ch in self.run.channels:
            if ch.source_type.upper() == "PMT".upper():
                self.pmt_channel = ch_num
                break
            ch_num += 1

        if self.pmt_channel == -1:
            print(" ")
            print("MAJOR ERROR")
            print("RUN " + str(self.run.name) + " contains no PMT channel.")
            print("Algorithm "+ str(self.name) + " will not run")
            print(" ") 
            sys.exit()

        
    def execute(self):
        baseline =  self.tr_store["baseline" + str(self.pmt_channel)]
        cumulative_adc = 0

        # loop through number of entries in array
        for idx in range(0, self.array_size):

            # If peak centred shift index idx to correct entry idx
            if self.peak_centred:
                entry_idx = idx + (self.tr_store["peak_location" + str(self.pmt_channel)] - self.before_peak)
            
            else:
                entry_idx = idx

            #load values, if statement catches values in waveform
            if (entry_idx >= 0) and (entry_idx < self.run.channels[0].record_length):
                adc = self.run.channels[self.pmt_channel].event.waveform[entry_idx]
                corrected_adc = baseline - adc
                cumulative_adc += corrected_adc

            # for out of range entries
            else:
                adc = baseline
                corrected_adc = 0
            
            # Add values to arrays
            if self.check_type:
                # signal events
                if self.tr_store["signal_type"]==1:
                    self.pr_store["avg_waveform"].Fill(idx, corrected_adc)
                    self.pr_store["avg_waveform_uncorrected"].Fill(idx, adc)
                    self.pr_store["avg_waveform_cumulative"].Fill(idx,cumulative_adc)
                    

                # background events
                else:
                    self.pr_store["avg_waveform_bkgrnd"].Fill(idx, corrected_adc)
                    self.pr_store["avg_waveform_uncorrected_bkgrnd"].Fill(idx, adc)
                    self.pr_store["avg_waveform_cumulative_bkgrnd"].Fill(idx, cumulative_adc)
                    
            
            else:
                self.pr_store["avg_waveform"].Fill(idx, corrected_adc)
                self.pr_store["avg_waveform_uncorrected"].Fill(idx, adc)
                self.pr_store["avg_waveform_cumulative"].Fill(idx, cumulative_adc)

        if self.check_type:
            if self.tr_store["signal_type"]==1:
                self.pr_store["num_avg_entries"] += 1
            else:
                self.pr_store["num_avg_entries_bkgrnd"] += 1

        else:        
            self.pr_store["num_avg_entries"] += 1

    def finalize(self):
        # Open root file
        outfilename = "avg_waveform-" + self.run.name + ".root"
        outfile = ROOT.TFile.Open(os.path.join(self.outpath, outfilename), "RECREATE")
        outfile.cd()

        # Normalise the historgrams if that is what youre doing
        if self.normalise:
            for idx in range(0, self.array_size):
                temp  = self.pr_store["avg_waveform"].GetBinContent(idx)/self.pr_store["num_avg_entries"]
                self.pr_store["avg_waveform"].SetBinContent(idx, temp)
                self.pr_store["avg_waveform"].SetBinError(idx, 0)

                temp  = self.pr_store["avg_waveform_uncorrected"].GetBinContent(idx)/self.pr_store["num_avg_entries"]
                self.pr_store["avg_waveform_uncorrected"].SetBinContent(idx, temp)
                self.pr_store["avg_waveform_uncorrected"].SetBinError(idx, 0)

                temp  = self.pr_store["avg_waveform_cumulative"].GetBinContent(idx)/self.pr_store["num_avg_entries"]
                self.pr_store["avg_waveform_cumulative"].SetBinContent(idx, temp)
                self.pr_store["avg_waveform_cumulative"].SetBinError(idx, 0)

                if self.check_type:
                    temp  = self.pr_store["avg_waveform_bkgrnd"].GetBinContent(idx)/self.pr_store["num_avg_entries_bkgrnd"]
                    self.pr_store["avg_waveform_bkgrnd"].SetBinContent(idx, temp)
                    self.pr_store["avg_waveform_bkgrnd"].SetBinError(idx, 0)

                    temp  = self.pr_store["avg_waveform_uncorrected_bkgrnd"].GetBinContent(idx)/self.pr_store["num_avg_entries_bkgrnd"]
                    self.pr_store["avg_waveform_uncorrected_bkgrnd"].SetBinContent(idx, temp)
                    self.pr_store["avg_waveform_uncorrected_bkgrnd"].SetBinError(idx, 0)

                    temp  = self.pr_store["avg_waveform_cumulative_bkgrnd"].GetBinContent(idx)/self.pr_store["num_avg_entries_bkgrnd"]
                    self.pr_store["avg_waveform_cumulative_bkgrnd"].SetBinContent(idx, temp)
                    self.pr_store["avg_waveform_cumulative_bkgrnd"].SetBinError(idx, 0)

        #write histograms to root_file
        self.pr_store["avg_waveform"].SetStats(False)
        self.pr_store["avg_waveform"].GetXaxis().SetTitle("Amplitude")
        self.pr_store["avg_waveform"].GetYaxis().SetTitle("Time [2ns]")
        self.pr_store["avg_waveform"].Write()

        self.pr_store["avg_waveform_uncorrected"].SetStats(False)
        self.pr_store["avg_waveform_uncorrected"].GetXaxis().SetTitle("Amplitude")
        self.pr_store["avg_waveform_uncorrected"].GetYaxis().SetTitle("Time [2ns]")
        self.pr_store["avg_waveform_uncorrected"].Write()

        self.pr_store["avg_waveform_cumulative"].SetStats(False)
        self.pr_store["avg_waveform_cumulative"].GetXaxis().SetTitle("Integrated Amplitude")
        self.pr_store["avg_waveform_cumulative"].GetYaxis().SetTitle("Time [2ns]")
        self.pr_store["avg_waveform_cumulative"].Write()

        if self.check_type:
            self.pr_store["avg_waveform_bkgrnd"].SetStats(False)
            self.pr_store["avg_waveform_bkgrnd"].GetXaxis().SetTitle("Amplitude")
            self.pr_store["avg_waveform_bkgrnd"].GetYaxis().SetTitle("Time [2ns]")
            self.pr_store["avg_waveform_bkgrnd"].Write()

            self.pr_store["avg_waveform_uncorrected_bkgrnd"].SetStats(False)
            self.pr_store["avg_waveform_uncorrected_bkgrnd"].GetXaxis().SetTitle("Amplitude")
            self.pr_store["avg_waveform_uncorrected_bkgrnd"].GetYaxis().SetTitle("Time [2ns]")
            self.pr_store["avg_waveform_uncorrected_bkgrnd"].Write()

            self.pr_store["avg_waveform_cumulative_bkgrnd"].SetStats(False)
            self.pr_store["avg_waveform_cumulative_bkgrnd"].GetXaxis().SetTitle("Integrated Amplitude")
            self.pr_store["avg_waveform_cumulative_bkgrnd"].GetYaxis().SetTitle("Time [2ns]")
            self.pr_store["avg_waveform_cumulative_bkgrnd"].Write()


        outfile.Close()

        



#####################################################################
#   ROOT  Fill Chain
#
#####################################################################
#   The following algorithm is used to write out variables in the PR and TR Store
#       into a chain to be saved as a root tree.
#   
#   This is a general function for all PMT algorithms, the type of chain to be filled is 
#       passed in the variable chain_type:
#           basic   - only saves CALC PMT VARS variables
#           spe     - saves basic & outputs of ROOT SPE for PMT and Laser
#           dr      - saves basic & outputs of dark rate algorithms
#
class ROOT_Fill_Chain(Algorithm):
    
    def __init__(self,  name = "ROOT_Fill_Chain", chain_type = "basic", output_chain=None, reload_chain = False):
        self.name=name
        self.chain_type = chain_type
        self.output_chain = output_chain
        self.reload_chain = reload_chain
        self.br_list = []
    

    def initialize(self):
        self.pr_store[self.output_chain].load_chain()

        # The following loads the correct list of chain variables to be filled per event, depending on algorithm type
        #   use of  .upper() compares both strings as upper cases to avoid any capitalisation issues
        # basic - stores only output of pmt_calc_vars
        if (self.chain_type.upper() == "pmt_basic".upper()):
            self.br_list = ["event_nr", "trigger_ts", "peak_height0","peak_location0","baseline0", "total_charge0","corrected_trigger_time"]
        
        if (self.chain_type.upper() == "pmt_spe".upper()):
            self.br_list = ["event_nr", "trigger_ts", "signal_type","peak_height_pmt", "peak_location_pmt", "baseline_pmt", "charge_pmt", "peak_location_laser", "peak_height_laser","corrected_trigger_time"]
        
        if (self.chain_type.upper() == "pmt_spe_psd".upper()):
            self.br_list = ["event_nr", "trigger_ts", "signal_type","peak_height_pmt", "peak_location_pmt", "baseline_pmt", "charge_pmt", "peak_location_laser", "peak_height_laser", "charge_extended", "charge_pre_peak", "charge_post_peak", "charge_tail", "mean_time", "moment_1", "moment_2", "moment_3", "moment_4", "samples_above_threshold","corrected_trigger_time"]
        
        if (self.chain_type.upper() == "pmt_spe_psd_radio".upper()):
            self.br_list = ["event_nr", "trigger_ts", "signal_type","peak_height_pmt", "peak_location_pmt", "baseline_pmt", "charge_pmt", "peak_location_laser", "peak_height_laser", "charge_extended", "charge_pre_peak", "charge_post_peak", "charge_tail", "mean_time", "moment_1", "moment_2", "moment_3", "moment_4", "samples_above_threshold","corrected_trigger_time", "peak_num", "peak_sum", "peak_avg", "freq_50_mavg_peak", "freq_200_mavg_peak", "freq_50_peak", "freq_200_peak"]
        
        if (self.chain_type.upper() == "pmt_dr".upper()):
            self.br_list = ["event_nr", "trigger_ts","peak_height_pmt", "peak_location_pmt", "baseline_pmt", "charge_pmt","corrected_trigger_time"]

        if (self.chain_type.upper() == "pmt_dr_psd".upper()):
            self.br_list = ["event_nr", "trigger_ts","peak_height_pmt", "peak_location_pmt", "baseline_pmt", "charge_pmt", "peak_height_pmt", "peak_location_pmt", "baseline_pmt", "charge_pmt", "charge_extended", "charge_pre_peak", "charge_post_peak", "charge_tail", "mean_time", "moment_1", "moment_2", "moment_3", "moment_4", "samples_above_threshold","corrected_trigger_time"]
        
        if (self.chain_type.upper() == "pmt_dr_psd_radio".upper()):
            self.br_list = ["event_nr", "trigger_ts","peak_height_pmt", "peak_location_pmt", "baseline_pmt", "charge_pmt", "peak_height_pmt", "peak_location_pmt", "baseline_pmt", "charge_pmt", "charge_extended", "charge_pre_peak", "charge_post_peak", "charge_tail", "mean_time", "moment_1", "moment_2", "moment_3", "moment_4", "samples_above_threshold","corrected_trigger_time", "peak_num", "peak_sum", "peak_avg", "freq_50_mavg_peak", "freq_200_mavg_peak", "freq_50_peak", "freq_200_peak"]


    def execute(self):
        cname = self.output_chain

        if self.run.form == "wd":
            #fill branch
            for br_name in self.br_list :
                #check branch exists in tr_store
                if br_name in self.tr_store.keys():
                    self.pr_store[cname].fill_branch(br_name, self.tr_store[br_name])

                # errors if branch not in tr_store
                if br_name in self.pr_store.keys():
                    sys.exit("ERROR: Branch: {} is in pr_store not tr_store".format(br_name))
                if (br_name not in self.tr_store.keys()) and (br_name not in self.pr_store.keys()):
                    sys.exit("ERROR: Branch: {} not found".format(br_name))

            self.pr_store[cname].fill_chain() 
 

    def finalize(self):
        tr_store_items = self.tr_store.keys()
        pr_store_items = self.pr_store.keys()

        # The following loads the correct list of chain variables to be filled at the end of the event, depending on algorithm type
        if (self.chain_type.upper() == "pmt_basic".upper()):
            self.br_list = ["event_count", "run_time_corrected"]
        
        if (self.chain_type.upper() == "pmt_spe".upper()):
            self.br_list = ["event_count", "run_time_corrected", "num_signal", "num_sidebar"]

        if (self.chain_type.upper() == "pmt_dr".upper()):
            self.br_list = ["event_count", "run_time_corrected"]

        if (self.chain_type.upper() == "pmt_spe_psd".upper()):
            self.br_list = ["event_count", "run_time_corrected", "num_signal", "num_sidebar"]

        if (self.chain_type.upper() == "pmt_spe_psd_radio".upper()):
            self.br_list = ["event_count", "run_time_corrected", "num_signal", "num_sidebar"]

        if (self.chain_type.upper() == "pmt_dr_psd".upper()):
            self.br_list = ["event_count", "run_time_corrected"]

        if (self.chain_type.upper() == "pmt_dr_psd_radio".upper()):
            self.br_list = ["event_count", "run_time_corrected"]

        cname = self.output_chain

        if self.run.form == "wd":
            #fill branch
            for br_name in self.br_list :
                #check branch exists in pr_store
                if br_name in self.pr_store.keys():
                    self.pr_store[cname].fill_branch(br_name, self.pr_store[br_name])

                # errors if branch not in pr_store
                if br_name in self.tr_store.keys():
                     sys.exit("ERROR: Branch: {} is in tr_store not pr_store in finalise".format(br_name))
                if (br_name not in self.tr_store.keys()) and (br_name not in self.pr_store.keys()):
                    sys.exit("ERROR: Branch: {} not found during finalise".format(br_name))

            self.pr_store[cname].fill_chain() 



#####################################################################
#   ROOT  DR ROC writeout
#       REQUIRES PMT CALC VARS 
#####################################################################
#   The following algorithm is used to writeout dark rate data to generate ROC curves
#   Data is written out into a text file located in outpath
#
class ROOT_DR_ROC_writeout(Algorithm):
    
    def __init__(self, name = "ROOT_DR_ROC_writeout", out_path = "/home/wdix/Documents/PMT_Analysis"):
        self.name=name
        self.out_path = out_path
        pass
    
    def initialize(self):
        pass
  
    def execute(self):
        pass
 
    def finalize(self):
        out_file  = open(os.path.join(self.out_path,"ROC_output" + str(self.run.name) + ".txt"),"a")
        out_file.write("Run: "+ str(self.run.name) + "\n")
        s_factor = self.run.channels[0].plot_scale_factor 
        
        for thresh in range(0,int(910*s_factor),int(30*s_factor)):
            bin_above = self.pr_store["dr_rate_hist"].FindFixBin(thresh,1)
            rate_above = self.pr_store["dr_rate_hist"].GetBinContent(bin_above)
            out_file.write("Threshold: " + str(thresh) +"\n")
            out_file.write("Dark Rate: " + str(rate_above) +"\n" +"\n")

        num_qbins = num_bins = self.pr_store["dr_charge_hist"].GetNbinsX()
        
        for q_thresh in range(0,int(18*s_factor)+1,1):
            bin_above = self.pr_store["dr_charge_hist"].FindFixBin(q_thresh,1)
            num_above=0
            for n in range(bin_above, num_qbins):
                num_above += self.pr_store["dr_charge_hist"].GetBinContent(n)

            out_file.write("Charge Threshold: " + str(q_thresh) +"\n")
            out_file.write("Dark Rate above charge threshold: " + str(num_above) +"\n" +"\n")



#####################################################################
#   ROOT  SPE ROC writeout
#       REQUIRES PMT CALC VARS 
#####################################################################
#   The following algorithm is used to writeout single photon data to generate ROC curves
#   Data is written out into a text file located in outpath
#
class ROOT_SPE_ROC_writeout(Algorithm):

    def __init__(self, name = "ROOT_SPE_Write_Tree", out_path = "/home/wdix/Documents/PMT_Analysis"):
        self.name=name
        self.out_path = out_path
        pass

    def initialize(self):
        pass

    def execute(self):
	    pass 

    def finalize(self):
        out_file  = open(os.path.join(self.out_path,"ROC_output" + str(self.run.name) + ".txt"),"a")
        out_file.write("Run: "+ str(self.run.name) + "\n")
        num_bins = self.pr_store["spe_peak_hist_sg"].GetNbinsX()
        s_factor = self.run.channels[0].plot_scale_factor 
        
        for thresh in range(0,int(910*s_factor),int(30*s_factor)):
            bin_above = self.pr_store["spe_peak_hist_sg"].FindFixBin(thresh,1)
            num_above = 0
            for n in range(bin_above, num_bins):
                num_above += self.pr_store["spe_peak_hist_sg"].GetBinContent(n)

            out_file.write("Threshold: " + str(thresh) +"\n")
            out_file.write("Number SPE above threshold: " + str(num_above) +"\n" +"\n")

        num_qbins = num_bins = self.pr_store["spe_charge_hist_sg"].GetNbinsX()
        
        for q_thresh in range(0,int(18*s_factor)+1,1):
            bin_above = self.pr_store["spe_charge_hist_sg"].FindFixBin(q_thresh,1)
            num_above=0
            for n in range(bin_above, num_qbins):
                num_above += self.pr_store["spe_charge_hist_sg"].GetBinContent(n)

            out_file.write("Charge Threshold: " + str(q_thresh) +"\n")
            out_file.write("Number SPE above charge threshold: " + str(num_above) +"\n" +"\n")






#####################################################################
#   ROOT Radio filter
#       REQUIRES PMT CALC VARS TO BE RUN FIRST
#####################################################################
#   This code generates a measure which can be used to identify if a pulse is due to radio noise.
#   it counts the number of baseline crossings in the peak centred charge window
#   it also returns the mean of the peaks around the baseline which for radio pulses should be 0

class ROOT_Radio_measure(Algorithm):

    def __init__(self, name = "ROOT_Radio_measure"):
        self.name=name
        self.pmt_ch_num = -1
        pass

    def initialize(self):
        # find PMT channel number
        ch_num = 0
        for ch in self.run.channels:
            if ch.source_type.upper() == "PMT".upper():
                self.pmt_channel = ch_num
                break
            ch_num += 1

        pass

    def execute(self, charge_window_width = CONSTANTS.CHARGE_WINDOW_WIDTH, prepeak_charge_window = CONSTANTS.PREPEAK_CHARGE_WINDOW, charge_window_start = -1):
       
        pmt_ch = self.run.channels[self.pmt_channel]
        pmt_waveform = pmt_ch.event.waveform
        Num_samples = pmt_ch.event.record_length

        # Create doubles to track number of peaks and
        num_peaks = 0
        peaks_sum = 0
        previous_adc = 0
        direction = 0 #(+VE for increasing, -VE for decreasing, 0 for initial)
        mini_wave = []
        mini_wave_x = []
        
        

        #check if window is valid
        pmt_peak_location = self.tr_store["peak_location_pmt"]#["peak_location" + str(self.pr_store["pmt_ch_num"])]
        if((prepeak_charge_window + CONSTANTS.NUM_BASELINE) < pmt_peak_location < (pmt_ch.event.record_length-(charge_window_width-prepeak_charge_window))):
            charge_window_start = pmt_peak_location - prepeak_charge_window
            
            # Create miniwaveform for FFT
            

            #loop over charge window
            for i in range(charge_window_start, charge_window_start + charge_window_width):
                #corrected_adc = self.tr_store["baseline" +str(self.pr_store["pmt_ch_num"])] - pmt_waveform[i]
                corrected_adc = self.tr_store["baseline_pmt"] - pmt_waveform[i]
                mini_wave.append(corrected_adc)
                mini_wave_x.append(2.0*i)
                
                # check for peak - if change from previous to current sample is opposite sign to direction
                if ((direction * (corrected_adc - previous_adc))<0):
                    num_peaks += 1
                    peaks_sum += previous_adc

                #Update values of previous adc and direction (if non zero)
                if abs(corrected_adc - previous_adc)>0.001:
                    direction = corrected_adc - previous_adc
                previous_adc = corrected_adc
                #note cannot update direction to be zero as this breaks the peak finder

        #write number, sum and avg to tree
        if num_peaks != 0:
            peaks_avg = peaks_sum/ num_peaks
        else:
            peaks_avg = 0

        # add to store
        self.tr_store["peak_num"] = num_peaks
        self.tr_store["peak_sum"] = peaks_sum
        self.tr_store["peak_avg"] = peaks_avg


        # determine FFT
        wave_f = rfft(pmt_waveform)
        wave_f_x = fftfreq(Num_samples, 2e-9)

        shrunk_wave_f = np.abs(wave_f[1:Num_samples//2])
        shrunk_wave_f_x = wave_f_x[1:Num_samples//2]

        from scipy.ndimage.filters import uniform_filter1d
        mavg_wave_f = uniform_filter1d(shrunk_wave_f, size = 11)
        mavg_wave_f_x = uniform_filter1d(shrunk_wave_f_x, size=11)
        avg_data = []

        for i in range(len(shrunk_wave_f_x)):
            if 0.7e8 < shrunk_wave_f_x[i] <1.6e8:
                avg_data.append(shrunk_wave_f[i])

        avg_amp = np.mean(avg_data)
        std_amp = np.std(avg_data)


        #determine 4 store variables for moving average peak and absolute peak
        calc_array = np.array(shrunk_wave_f)
        calc_mavg_array = np.array(mavg_wave_f)
        #calc_array =calc_array - avg_amp
        ##calc_array = calc_array/std_amp
        #calc_mavg_array/std_amp

        temp_mvg_50_peak = 0
        temp_mvg_200_peak = 0
        temp_50_peak = 0
        temp_200_peak = 0

        #Find peak value in range 40-65 MHz or 170 MHz to 200 MHz
        for i in range(len(calc_array)):
            if 0.4e8 < shrunk_wave_f_x[i]< 0.65e8:
                #print(calc_array[i])
                if ((calc_array[i]-avg_amp)/std_amp) > temp_50_peak:
                    temp_50_peak = ((calc_array[i]-avg_amp)/std_amp)

            if 1.7e8 < shrunk_wave_f_x[i]< 2.0e8:
                #print(calc_array[i])
                if ((calc_array[i]-avg_amp)/std_amp) > temp_200_peak:
                    temp_200_peak = ((calc_array[i]-avg_amp)/std_amp)
            

        #Find peak value in range 40-65 MHz or 170 MHz to 200 MHz
        for i in range(len(calc_mavg_array)):
            if 0.4e8 < mavg_wave_f_x[i] < 0.65e8:
                if ((calc_mavg_array[i]-avg_amp)/std_amp) > temp_mvg_50_peak:
                    temp_mvg_50_peak = ((calc_mavg_array[i]-avg_amp)/std_amp)
            if 1.7e8 < mavg_wave_f_x[i]< 2.0e8:
                if ((calc_mavg_array[i]-avg_amp)/std_amp) > temp_mvg_200_peak:
                    temp_mvg_200_peak = ((calc_mavg_array[i]-avg_amp)/std_amp)  
            

        # add to store
        self.tr_store["freq_50_mavg_peak"] = temp_mvg_50_peak
        self.tr_store["freq_200_mavg_peak"] = temp_mvg_200_peak
        self.tr_store["freq_50_peak"] = temp_50_peak
        self.tr_store["freq_200_peak"] = temp_200_peak

        #plt.semilogy(wave_f_x[1:Num_samples//2], np.abs(wave_f[1:Num_samples//2]), '-b')
        #plt.show()
        if(False):
            wave_x = np.linspace(0, 2.0* Num_samples, num=Num_samples, endpoint=False)
            fig, axs = plt.subplots(2,1)
            axs[0].plot(wave_x, pmt_waveform)
            axs[0].set_xlabel('Time [ns]')
            axs[0].set_ylabel('Raw Waveform')
            axs[0].grid(True)

            axs[1].plot(mavg_wave_f_x[1:Num_samples//2], mavg_wave_f[1:Num_samples//2], '-g')
            axs[1].plot(shrunk_wave_f_x, shrunk_wave_f, '-b')
            axs[1].plot([0,shrunk_wave_f_x[-1]],[avg_amp, avg_amp], '-r')
            axs[1].plot([0,shrunk_wave_f_x[-1]],[avg_amp +std_amp, avg_amp+std_amp], '--r')
            axs[1].plot([0,shrunk_wave_f_x[-1]],[avg_amp+2.0*std_amp, avg_amp+2.0*std_amp], '-.r')
            axs[1].set_xlabel('Frequency [Hz]')
            axs[1].set_ylabel('Amplitude')

            fig.set_size_inches(18.5,10.5, forward = True)

            
            plt.show()

    def finalize(self):
        pass
