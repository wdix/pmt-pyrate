from pyrate.classes import Algorithm
from utils import typecast
import numpy as np
import array
import statistics
import pmt_constants as CONSTANTS
import os

description = "Collection of algorithms written to process PMT data in the pyrate frame work"


#####################################################################
#   PMT CALC VARS
#####################################################################
#   The following algorithm calculates a several general variables that may be used in other algorithms
#   as such where possible it will use the same names as in the var_algs.CalcVars() algorithm
#
#   Variables created in the permanant/transient store 
#       "event_count"           pr_store    Total number of recorded events in run
#       "avg_event_rate"        pr_store    Average event rate (with no event selection)
#       "run_start_time"        pr_store    Run start time (in digitiser clock time 8ns units)
#       "run_end_time_corrected" pr_store   Run end time (in digitiser clock time 8ns units) corrected for clock overflow
#       "clock_overflow_counter" pr_store   Counts number of clock overflows (clock overflow occurs every ~17s or 2^31 clock units)
#       "previous_event_time"   pr_store    records time of previous event (used to check for clock overflow)
#       "run_time_corrected"    pr_store    run time in seconds using the overflow corrected time
# 
#       "record_length"         tr_store    Event length in samples
#       "trigger_ts"            tr_store    Corrected trigger time stamp (in clock units)
#       "corrected_trigger_time" tr_store   Corrected trigger time stamp in clock units as a double precision float
#       "event_nr"              tr_store    Event number (starts with 1, ie. event index +1)
# 
#   Channel Variables - [idx] is channel index, replace square brackets with index number ie. waveform[idx] -> waveform0
#       "peak_height[idx]"      tr_store    Peak height (above baseline) expressed as a positive number
#       "peak_location[idx]"    tr_store    Peak location (in samples)
#       "baseline[idx]"         tr_store    Baseline calculated over 40 samples (currently hardcoded)
#       "total_charge[idx]"     tr_store    Total waveform charge (for all samples)    
#
class CalcPMTVars(Algorithm):
    
    def __init__(self, name = "CalcPMTVars"):
        self.name=name
    
    
    def initialize(self):
        if self.run.form == "wc":
            print(" ")
            print("Data collected from wavecatcher is currently not supported by the " +str(self.name) + " algorithm.")
            print(" ")
        
        if self.run.form == "wd":
            #Create data structures
            #-----------------------------------------
            
            #Event level variables
            #------------------------------------------
            self.pr_store["event_count"]            = 0
            #The following variables are used in calculating run duration, rates and corrected trigger times
            #Note: run_start_time and run_end_time_corrected are taken from the digitiser clock (8 ns), to convert to seconds *8e-9
            self.pr_store["avg_event_rate"]         = 0
            self.pr_store["run_start_time"]         = 0
            self.pr_store["clock_overflow_counter"] = 0
            self.pr_store["previous_event_time"]    = 0
            self.pr_store["run_end_time_corrected"] = 0    #corrected referes to fixing issues with trigger clock overflow
            self.pr_store["run_time"] = 0
            
            #Channel level variables
            #-------------------------------------------
            ch_num = 0
            for ch in self.run.channels:
                print("Channel " + str(ch_num) +" is from a " + ch.source_type)
                ch_num += 1

    
    def execute(self):
        
        if self.run.form == "wc":
            pass
        
        if self.run.form == "wd":
            #Event Data
            #---------------------------------
            self.tr_store["trigger_ts"]    = int(self.run.channels[0].event.header["Trigger Time Stamp"])
            self.tr_store["record_length"] = self.run.channels[0].event.record_length
            self.tr_store["event_nr"]  = self.run.channels[0].event.idx_event + 1
            self.tr_store["corrected_trigger_time"] = -1.0
            
            #The following statement is used to calculate the trigger rate other rates can be calcluated with other algorithms
            self.pr_store["event_count"] += 1
            
            #The following is used to determine run start and stop times, and to correct for overflow in trigger clock
            if self.run.channels[0].event.idx_event==0: # Start run time
                self.pr_store["run_start_time"] = self.tr_store["trigger_ts"]
            if self.tr_store["trigger_ts"] < self.pr_store["previous_event_time"]:  #Overflow checker
                self.pr_store["clock_overflow_counter"] +=1
            self.pr_store["previous_event_time"] = self.tr_store["trigger_ts"]

            #store corrected trigger time as trigger time
            corrected_trigger_time = self.tr_store["trigger_ts"] + self.pr_store["clock_overflow_counter"] * 2**31
            self.tr_store["trigger_ts"] = corrected_trigger_time

            #save trigger time as float
            self.tr_store["corrected_trigger_time"] = float(corrected_trigger_time)

            #Channel Loop
            #------------------------------
            ch_num=0
            for ch in self.run.channels:
                # general variables
                waveform = ch.event.waveform
                self.tr_store["peak_height" + str(ch_num)] = 0
                self.tr_store["peak_location" + str(ch_num)] = 0
                
                #Peak Finding loop
                # The following loop is for NEGATIVE polarity signals
                if ch.source_type.upper() in {"PMT", "DIODE"}:
                    self.tr_store["peak_height" + str(ch_num)] = 20000
                    for i in range (0,ch.event.record_length):
                        if waveform[i] < self.tr_store["peak_height" + str(ch_num)]:
                            self.tr_store["peak_height" + str(ch_num)] = waveform[i]
                            self.tr_store["peak_location" + str(ch_num)] = i
                
                # The following loop is for POSITIVE polarity signals        
                if ch.source_type.upper() in {"LASER"}:
                    for i in range (0,ch.event.record_length):
                        if waveform[i] > self.tr_store["peak_height" + str(ch_num)]:
                            self.tr_store["peak_height" + str(ch_num)] = waveform[i]
                            self.tr_store["peak_location" + str(ch_num)] = i
                        
                # Baseline Calculating Loop
                if ch.source_type.upper() in {"PMT", "DIODE"}:
                    self.tr_store["baseline" + str(ch_num)]     = 0.000
                    for i in range (0,CONSTANTS.NUM_BASELINE):
                        self.tr_store["baseline" + str(ch_num)] += ( 1.000*waveform[i])/(1.000*CONSTANTS.NUM_BASELINE)
                        
                    #correct peak height to be peak height above baseline as a positive value
                    temp_peak = self.tr_store["peak_height" + str(ch_num)]
                    self.tr_store["peak_height" + str(ch_num)] = self.tr_store["baseline" + str(ch_num)] - temp_peak
                
                # Pulse charge for PMT and Diode
                if ch.source_type.upper() in {"PMT"}:
                    temp_charge = 0.000
                    for i in range (0,ch.event.record_length):
                        temp_charge += - waveform[i] + self.tr_store["baseline" + str(ch_num)]
                    
                    self.tr_store["total_charge" + str(ch_num)] = temp_charge * CONSTANTS.CHARGE_CONSTANT
                

                #Source spefic variables
                #if ch.source_type.upper() == "PMT":
                    
                #if ch.source_type.upper() == "LASER":
                    
                #if ch.source_type.upper() == "DIODE":
                    
                #End of ch loop, updates channel index
                ch_num += 1


    def finalize(self):
        
        if self.run.form == "wc":
            pass
        
        if self.run.form == "wd":
            #calculates end time and event rate
            num_overflow = int(self.pr_store["clock_overflow_counter"])
            end_time = int(self.pr_store["previous_event_time"])
            num_events = self.pr_store["event_count"]
            self.pr_store["run_end_time_corrected"] = end_time + 2**31 * num_overflow
            run_time = (self.pr_store["run_end_time_corrected"] - self.pr_store["run_start_time"])*0.000000008
            self.pr_store["run_time_corrected"] = run_time

            if run_time < 0.01: #Error Flag
                print(" ")
                print("error in run start and end time")
                print(" ")
            
            else:
                self.pr_store["avg_trigger_rate"] = (1.0000*num_events)/(1.00000*run_time)
                if(self.pr_store["avg_event_rate"]<0.5):
                    print(" ")
                    print("WARNING - avg event rate is less than 0.5 hz therefore overflow count and run rate may be unreliable")
                    print(" ")
                print(" ")
                print("Run: " + str(self.run.name))
                print("Run time: " + str(run_time) +" s")
                print("Event Count: " +str(self.pr_store["event_count"]))
                print("Avg. event rate: " + str(self.pr_store["avg_event_rate"]) +" Hz")
            



#####################################################################
#   Calc Pulse Shape
#####################################################################
#   DEPENDENT ON PMT CALC VARS
#   The following algorithm calculates pulse shape variables for PMT pulses connected to scintillators
#   it is currently a work in progress
#
#   Variables created in the permanant/transient store 
# 
#   Channel Variables - [idx] is channel index, replace square brackets with index number ie. waveform[idx] -> waveform0
#
##
class Calc_Pulse_Shape_Vars(Algorithm):
    
    def __init__(self, name = "Calc_Pulse_Shape_Vars"):
        self.name=name
        pass
    
    def initialize(self):
        if self.run.form == "wc":
            print(" ")
            print("Data collected from wavecatcher is currently not supported by the Laser Peak algorithm.")
            print(" ")
        pass
  
    def execute(self, trigger_threshold = CONSTANTS.DEFAULT_TRIGGER_THRESHOLD):
        if self.run.form == "wc":
            pass
        
        if self.run.form == "wd":
            for ch in self.run.channels:
                waveform = ch.event.waveform
                pass
 
    def finalize(self):
        pass




#####################################################################
#   SPE (Single PhotoElectron) Charecterisation Algorithm
#       REQUIRES PMT CALC VARS TO BE RUN FIRST
#####################################################################
#   The following algorithm is used in single photoelectron characterisation measurements
#       it checks for a laser signal in a suitable time window (with a sideband region also identified), 
#       recalculates the pulse charge and and expresses the variables in a consistant manner for root histograms.
#       Pulse charge is either fixed or floating but window must not overlap with baseline calculation
#
#   Individual event is classified as (stored in self.tr_store["signal_type"])
#       0 - non signal event
#       1 - signal event
#       2 - sidebar event
#
#   The Algorithm has several optional arguments (default values are defined in pmt_constants.py)
#       signal_region_start     - start location of the signal acceptance window (in samples)
#       signal_region_width     - width of signal and sidebar region (insamples)
#       sidebar_region_delay    - distance between end of signal region and start of sidebar region
#       charge_window_width     - width of window used to determine SPE pulse charge
#       prepeak_charge_window   - number of samples of charge window taken before the waveform peak if window is allowed to move
#       charge_window_start     - gives start of charge window if fixed window location is used (-1 if not used, which is default)
#
#   Variables created in the permanant/transient store 
#       "laser_ch_num"      pr_store    Channel number of laser channel
#       "pmt_ch_num"        pr_store    Channel number of pmt channel
#       "num_signal"        pr_store    Number of signal events
#       "num_sidebar"       pr_store    Number of sidebar events
#       "window_error"      pr_store    Flag for error in hardcoded window error (1 = error)
#       "signal_type"       tr_store    event classification
#       "signal_charge"     tr_store    charge of signal events (not used for non-signal events)
#       "sidebar_charge"    tr_store    charge of sidebar events (not used for non-sidebar events)
#
class SPE_Characterisation(Algorithm):
    
    def __init__(self, name = "SPE_Characterisation"):
        self.name=name
        pass
    

    def initialize(self, signal_region_start = CONSTANTS.SIGNAL_REGION_START, signal_region_width = CONSTANTS.SIGNAL_REGION_WIDTH, sidebar_region_delay = CONSTANTS.SIDEBAR_REGION_DELAY, charge_window_width = CONSTANTS.CHARGE_WINDOW_WIDTH, charge_window_start = -1):
        if self.run.form == "wc":
            print(" ")
            print("Data collected from wavecatcher is currently not supported by the " +str(self.name) + " algorithm.")
            print(" ")

        if self.run.form == "wd":
            #determine which channel is laser / PMT
            ch_num = 0
            for ch in self.run.channels:
                if ch.source_type.upper() == {"LASER"}:
                    self.pr_store["laser_ch_num"] = ch_num
                if ch.source_type.upper() == {"PMT"}:
                    self.pr_store["pmt_ch_num"] = ch_num
                ch_num += 1

            self.pr_store["num_signal"] = 0
            self.pr_store["num_sidebar"] =0

            #checks that hard coded windows are valid (no element will be out of range)
            event_length =  self.run.channels[0].event.record_length
            #Laser signal and sidebar window
            self.pr_store["window_error"] = 0
            if (signal_region_start<0):
                self.pr_store["window_error"] = 1
                print(" ")
                print("ERROR: Laser signal region is out of range for SPE characterisation - under")
                print(" ")
                pass
            if (signal_region_start+signal_region_width>=event_length):
                self.pr_store["window_error"] = 1
                print(" ")
                print("ERROR: Laser signal region is out of range for SPE characterisation - over")
                print(" ")
                pass
            if (signal_region_start+signal_region_width+sidebar_region_delay<0):
                self.pr_store["window_error"] = 1
                print(" ")
                print("ERROR: Laser sidebar region is out of range for SPE characterisation - under")
                print(" ")
                pass
            if (signal_region_start+signal_region_width+sidebar_region_delay+signal_region_width>=event_length):
                self.pr_store["window_error"] = 1
                print(" ")
                print("ERROR: Laser sidebar region is out of range for SPE characterisation - over")
                print(" ")
                pass
            #Check that hardcode charge window is valid (if used)
            if (charge_window_start>=0):
                if (charge_window_start<CONSTANTS.NUM_BASELINE):
                    self.pr_store["window_error"] = 1
                    print(" ")
                    print("ERROR: Pulse charge region is out of range for SPE characterisation - under")
                    print(" ")
                    pass
                if (charge_window_start+charge_window_width>=event_length):
                    self.pr_store["window_error"] = 1
                    print(" ")
                    print("ERROR: Pulse charge region is out of range for SPE characterisation - over")
                    print(" ")
                    pass

        pass
  
  
    def execute(self, signal_region_start = CONSTANTS.SIGNAL_REGION_START, signal_region_width = CONSTANTS.SIGNAL_REGION_WIDTH, sidebar_region_delay = CONSTANTS.SIDEBAR_REGION_DELAY, charge_window_width = CONSTANTS.CHARGE_WINDOW_WIDTH, prepeak_charge_window = CONSTANTS.PREPEAK_CHARGE_WINDOW, charge_window_start = -1):
        if self.run.form == "wc":
            pass
        
        if self.run.form == "wd":
            # skips events if hardcoded window is not valid
            if (self.pr_store["window_error"]==1):
                pass

            # the following checks for laser in signal or sidebar region
            laser_ch = self.run.channels[self.pr_store["laser_ch_num"]]
            laser_waveform = laser_ch.event.waveform
            self.tr_store["laser_peak_location"] = self.tr_store["peak_location" + str(self.pr_store["laser_ch_num"])]
            #classify as signal or sidebar region
            self.tr_store["signal_type"] = 0
            if laser_waveform[self.tr_store["laser_peak_location"]]> CONSTANTS.LASER_THRESHOLD: #forces laser pulse to be above threshold
                if (signal_region_start <= self.tr_store["laser_peak_location"] < signal_region_start + signal_region_width):
                    self.tr_store["signal_type"]=1
                sidebar_region_start = signal_region_start + signal_region_width + sidebar_region_delay
                if (sidebar_region_start <= self.tr_store["laser_peak_location"] < sidebar_region_start + signal_region_width):
                    self.tr_store["signal_type"]=2

            #calculate integrated charge for signal and sidebar events
            pmt_ch = self.run.channels[self.pr_store["pmt_ch_num"]]
            if self.tr_store["signal_type"]>0:
                pmt_waveform = pmt_ch.event.waveform
                #hardcoded charge window
                temp_charge = 0.000
                if charge_window_start >= 0:
                    for i in range(charge_window_start, charge_window_start + charge_window_width):
                        temp_charge += self.tr_store["baseline" +str(self.pr_store["pmt_ch_num"])] - pmt_waveform[i]
                #peak centred charge window
                if charge_window_start < 0:
                    #check if window is valid
                    pmt_peak_location = self.tr_store["peak_location" + str(self.pr_store["pmt_ch_num"])]
                    if(prepeak_charge_window + CONSTANTS.NUM_BASELINE < pmt_peak_location < pmt_ch.event.record_length-(charge_window_width-prepeak_charge_window)):
                        charge_window_start = pmt_peak_location - prepeak_charge_window
                        for i in range(charge_window_start, charge_window_start + charge_window_width):
                            temp_charge += self.tr_store["baseline" +str(self.pr_store["pmt_ch_num"])] - pmt_waveform[i]
                    else:
                        pass

            # express charge as either signal or sidebar for histogram
            if self.tr_store["signal_type"]==1: #is signal
                self.tr_store["signal_charge"] = temp_charge * CONSTANTS.CHARGE_CONSTANT
                self.pr_store["num_signal"] += 1
            elif self.tr_store["signal_type"] == 2: #is sidebar
                self.tr_store["sidebar_charge"] = temp_charge * CONSTANTS.CHARGE_CONSTANT
                self.pr_store["num_sidebar"] += 1
        pass
 

    def finalize(self):
        run_time = (self.pr_store["run_end_time_corrected"] - self.pr_store["run_start_time"])*0.000000008
        signal_rate = 1.000*self.pr_store["num_signal"] / run_time
        sidebar_rate = 1.000*self.pr_store["num_signal"] / run_time

        print(" ")
        print("SPE Algorithm Output - Run: " + str(self.run.name))
        print("--------------------------------------------------------------")
        print("Signal Rate:  " + str(signal_rate) + str(" Hz"))
        print("Sidebar Rate: " + str(sidebar_rate) + str(" Hz"))
        print(" ")
        pass



#####################################################################
#   Dark Rate Algorithm
#       REQUIRES PMT CALC VARS TO BE RUN FIRST
#####################################################################
#   The following algorithm is used to measure the darkrate, with an option to generate a cumulative plot of dark rate as a function of thershold.
#   Currently the threshold requirement is simply a single sample peak above threshold 
#
#   The Algorithm has several optional arguments (default values are defined in pmt_constants.py)
#       dark_rate_threshold     - Threshold used for dark rate
#
#   Variables created in the permanant/transient store 
#       "total_dark_events" pr_store    total number of dark events (across multiple channels)
#       "total_dark_rate"   pr_store    total dark rate
#       "is_dark_rate"      tr_store    flag for histogram info
#
#   Channel Variables - [idx] is channel index, replace square brackets with index number ie. waveform[idx] -> waveform0
#       "num_dark_events[idx]"  pr_store    number of dark events for channel [idx]
#       "dark_rate[idx]"        pr_store    dark rate for channel [idx]
#       
##
class Dark_Rate(Algorithm):
    def  __init__(self, name = "Dark_Rate", dark_rate_threshold = CONSTANTS.DARK_RATE_THRESHOLD, out_path = "/home/wdix/Documents/PMT_Analysis"):
        self.name=name
        self.dr_threshold=dark_rate_threshold
        #this is the output path for the file which records all text output
        self.out_path = out_path
        pass


    def initialize(self):

        if self.run.form == "wc":
            print(" ")
            print("Data collected from wavecatcher is currently not supported by the " +str(self.name) + " algorithm.")
            print(" ")
            pass

        self.pr_store["total_dark_events"]  = 0
        self.pr_store["total_dark_rate"]    = 0

        ch_num = 0
        for ch in self.run.channels:
            if ch.source_type.upper() in {"PMT"}:
                self.pr_store["num_dark_events"+ str(ch_num)] = 0
                self.pr_store["dark_rate"+ str(ch_num)] = 0
            ch_num += 1
        
            pass


    def execute(self):
        ch_num = 0
        temp_global_is_event = 0
        self.tr_store["is_dark_rate"] = 0

        if self.run.form == "wd":
            for ch in self.run.channels:
                if ch.source_type.upper() in {"PMT"}:
                    if self.tr_store["peak_height"+ str(ch_num)] >self.dr_threshold:
                        self.pr_store["num_dark_events"+ str(ch_num)] += 1
                        temp_global_is_event +=1
                
                ch_num += 1
            
            if temp_global_is_event > 0:
                self.pr_store["total_dark_events"] += 1
                self.tr_store["is_dark_rate"] = 1
        pass


    def finalize(self):
        if self.run.form == "wd":
            run_time = (self.pr_store["run_end_time_corrected"] - self.pr_store["run_start_time"])*0.000000008
            
            # This is the output file used to write out dark rate information to a text file
            # out_file  = open(os.path.join(self.out_path,"Dark_Rate_Algorithm_Output.txt"),"a")

            print(" ")
            print("Global Dark Rate:")
            print("---------------------------------------------------------")
            temp_dark_rate = 1.000*self.pr_store["total_dark_events"]/(1.00*run_time)
            self.pr_store["total_dark_rate"] = temp_dark_rate
            print("Number of dark events: " +str(self.pr_store["total_dark_events"]))
            print("Run time: " +str(run_time))
            print("Dark rate: " +str(temp_dark_rate))
            print(" ")

            #out_file.write("Run: "+ str(self.run.name) + "\n")
            #out_file.write("Threshold: " + str(self.dr_threshold) +"\n")
            #out_file.write("Global Dark Rate \n")
            #out_file.write("---------------------------------------------------------\n")
            #out_file.write("Number of dark events: " +str(self.pr_store["total_dark_events"]) + "\n")
            #out_file.write("Run time: " +str(run_time) + "\n")
            #out_file.write("Dark rate: " +str(temp_dark_rate) + " Hz \n \n")

            #out_file.write("Channel Dark Rate: \n")
            #out_file.write("---------------------------------------------------------\n")


            ch_num = 0
            print("Channel Dark Rate:")
            print("---------------------------------------------------------")
            for ch in self.run.channels:
                if ch.source_type.upper() in {"PMT"}:
                    temp_dark_rate = 1.000*self.pr_store["num_dark_events"+ str(ch_num)]/(1.00*run_time)
                    self.pr_store["dark_rate"+ str(ch_num)] = temp_dark_rate

                    print("Channel " + str(ch_num))
                    print("    Number of dark events: " +str(self.pr_store["num_dark_events"+ str(ch_num)]))
                    print("    Dark rate: " +str(temp_dark_rate) +" Hz")
                    print(" ")

                    #out_file.write("Channel " + str(ch_num)+"\n")
                    #out_file.write("    Number of dark events: " +str(self.pr_store["num_dark_events"+ str(ch_num)])+"\n")
                    #out_file.write("    Dark rate: " +str(temp_dark_rate) +" Hz \n \n")
                
                ch_num += 1
            
        pass
            





#####################################################################
#   Emulate Trigger
#####################################################################
#   DEPENDENT ON PMT CALC VARS
#   The following algorithm simulates an offline trigger (which makes an amplitude based cut)
#       which can be used to process data taken with a low threshold trigger
#   it is currently a work in progress
#
#   The threshold used in the trigger emulated cut is either passed to the function as the variable 
#       "trigger_threshold" or if no value is given it defaults to a value specified in pmt_constants.py
#   
#   Variables created in the permanant/transient store 
#       "corrected_event_count" pr_store    Number of events that pass the threshold cut
#       "common_event_trigger"  tr_store    Flag for if event passes trigger logic
#
#   Channel Variables - [idx] is channel index, replace square brackets with index number ie. waveform[idx] -> waveform0
#       "offline_trigger[idx]"  tr_store    Flag for if channel [idx] passes the emulated trigger cut (1 = accepted event)
##
class Emulated_Trigger(Algorithm):
    def __init__(self, name = "Emulated_Trigger"):
        self.name=name
        pass
    

    def initialize(self):
        if self.run.form == "wc":
            print(" ")
            print("Data collected from wavecatcher is currently not supported by the " +str(self.name) + " algorithm.")
            print(" ")
            pass

        self.pr_store["corrected_event_count"]         = 0
        
        ch_num=0
        for ch in self.run.channels:
            self.pr_store["corrected_event_count" + str(ch_num)] = 0
            ch_num+=1
        
        pass
  

    def execute(self, trigger_threshold = CONSTANTS.DEFAULT_TRIGGER_THRESHOLD):
        self.tr_store["common_event_trigger"]=0
        ch_num=0
        #Loop over channels to generate channel triggers
        for ch in self.run.channels:
            if ch.source_type.upper() in {"PMT"}:
                self.tr_store["offline_trigger"+ str(ch_num)] = 0
                waveform_temp = ch.event.waveform
                # The following record streaks above threshold
                current_streak = 0
                longest_streak = 0
                
                if self.tr_store["peak_height"+ str(ch_num)] >trigger_threshold:
                    # statement ensuring that event index will stay valid
                    if ((self.tr_store["peak_location"+ str(ch_num)] > CONSTANTS.PREPEAK_TRIGGER_WINDOW) and (self.tr_store["peak_location"+ str(ch_num)] < ch.event.record_length - CONSTANTS.POSTPEAK_TRIGGER_WINDOW)):
                        # loop goes from pre-peak to post peak
                        for i in range(self.tr_store["peak_location"+ str(ch_num)] - CONSTANTS.PREPEAK_TRIGGER_WINDOW, self.tr_store["peak_location"+ str(ch_num)]+CONSTANTS.POSTPEAK_TRIGGER_WINDOW):
                            adc = - (waveform_temp[i] - self.tr_store["baseline"+str(ch_num)])
                            # check if sample is above trigger threshold
                            if adc >= trigger_threshold:
                                current_streak = 0
                                while( adc>= trigger_threshold):
                                    current_streak += 1
                                    i=i+1
                                    if i >= ch.event.record_length:
                                        break
                                    adc = - (waveform_temp[i] - self.tr_store["baseline"+str(ch_num)])
                                if current_streak > longest_streak:
                                    longest_streak = current_streak
                            
                    
                    
                    elif self.tr_store["peak_location"+ str(ch_num)] <= CONSTANTS.PREPEAK_TRIGGER_WINDOW:
                        for i in range(0,CONSTANTS.PREPEAK_TRIGGER_WINDOW+CONSTANTS.POSTPEAK_TRIGGER_WINDOW):
                            adc = - (waveform_temp[i] - self.tr_store["baseline" + str(ch_num)])
                            # check if sample is above trigger threshold
                            if adc >= trigger_threshold:
                                current_streak = 0
                                while( adc>= trigger_threshold):
                                    current_streak += 1
                                    i=i+1
                                    if i >= ch.event.record_length:
                                        break
                                    adc = - (waveform_temp[i] - self.tr_store["baseline"+str(ch_num)])
                                if current_streak > longest_streak:
                                    longest_streak = current_streak
                    
                    elif self.tr_store["peak_location"+ str(ch_num)] >= ch.event.record_length:
                        for i in range(ch.event.record_length -(CONSTANTS.PREPEAK_TRIGGER_WINDOW+CONSTANTS.POSTPEAK_TRIGGER_WINDOW), ch.event.record_length):
                            adc = - (waveform_temp[i] - self.tr_store["baseline" + str(ch_num)])
                            # check if sample is above trigger threshold
                            if adc >= trigger_threshold:
                                current_streak = 0
                                while( adc>= trigger_threshold):
                                    current_streak += 1
                                    i=i+1
                                    if i >= ch.event.record_length:
                                        break
                                    adc = - (waveform_temp[i] - self.tr_store["baseline"+str(ch_num)])
                                if current_streak > longest_streak:
                                    longest_streak = current_streak
                                
                if longest_streak >= CONSTANTS.NUM_ABOVE_THRESHOLD:
                    self.tr_store["offline_trigger"+ str(ch_num)] = 1
                    self.pr_store["corrected_event_count" + str(ch_num)] += 1

            #The following loop is for debugging purposes
            if self.tr_store["offline_trigger"+ str(ch_num)]==0:
                print(" ")
                print("Event: "+str(self.run.channels[0].event.idx_event))
                print("Channel: " +str(ch_num))
                print("Baseline = "+str(self.tr_store["baseline" + str(ch_num)]))
                print("Waveform:")
                print(waveform_temp)
                print("Peak height = "+str(self.tr_store["peak_height"+ str(ch_num)]))
                print("Number above threshold = "+str(longest_streak))
                print(" ")
            
            #move to next channel
            ch_num+=1
        
        #Additional loop over channel to generate global trigger
        ch_num=0
        temp_num_trigger =0
        for ch in self.run.channels:
            if self.tr_store["offline_trigger"+ str(ch_num)] == 1:
                temp_num_trigger +=1
            ch_num+=1
            
        if temp_num_trigger >= 1:
            self.pr_store["corrected_event_count"] += 1
            self.tr_Store["common_event_trigger"] = 1
        
        pass
 

    def finalize(self):
        if self.run.form == "wd":
            run_time = (self.pr_store["run_end_time_corrected"] - self.pr_store["run_start_time"])*0.000000008
            print("Corrected event number: " + str(self.pr_store["corrected_event_count"]))
            print("Corrected event rate: " + str(self.pr_store["corrected_event_count"]/run_time) +" Hz")
            print(" ")
        pass


