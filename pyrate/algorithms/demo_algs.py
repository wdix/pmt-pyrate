from pyrate.classes import Algorithm
from utils import typecast
import numpy as np
import array
import statistics
#from scipy import stats


description="""
Algorithms for testing and demo.
"""

class FillPlot(Algorithm):

  def __init__(self, name="FillPlot", plt = None, hist = None):
    
    self.name       = name
    self.plt        = plt
    self.hist       = hist

  def initialize(self):
    pass

  def execute(self):
    
    if self.hist: 
      h_name = self.hist.GetName()
      self.hist.Fill(self.tr_store[h_name])

  def finalize(self):

    if self.plt: 
      self.plt.ylim(0., 22000.)
      self.plt.xlim(40., len(self.pr_store["wf_min_ch0"])+40)

      self.plt.plot(xrange(len(self.pr_store["wf_min_ch0"])), self.pr_store["wf_min_ch0"])
      self.plt.plot(xrange(len(self.pr_store["wf_min_ch1"])), self.pr_store["wf_min_ch1"])
      self.plt.plot(xrange(len(self.pr_store["wf_min_ch2"])), self.pr_store["wf_min_ch2"])
  


class CalcMin(Algorithm):

  def __init__(self, name="CalcMin"):
    
    self.name            = name

  def initialize(self):

    self.pr_store["min_ch0"] = 20000.
    self.pr_store["min_ch1"] = 20000.   
    self.pr_store["min_ch2"] = 20000.

    self.pr_store["wf_min_ch0"] = []
    self.pr_store["wf_min_ch1"] = []   
    self.pr_store["wf_min_ch2"] = []


  def execute(self):

    if min(self.run.channels[0].event.waveform) < self.pr_store["min_ch0"]:
      self.pr_store["wf_min_ch0"] = self.run.channels[0].event.waveform
      self.pr_store["min_ch0"] = min(self.run.channels[0].event.waveform)
    
    if min(self.run.channels[1].event.waveform) < self.pr_store["min_ch1"]:
      self.pr_store["wf_min_ch1"] = self.run.channels[1].event.waveform
      self.pr_store["min_ch1"] = min(self.run.channels[1].event.waveform)
    
    if min(self.run.channels[2].event.waveform) < self.pr_store["min_ch2"]:
      self.pr_store["wf_min_ch2"] = self.run.channels[2].event.waveform
      self.pr_store["min_ch2"] = min(self.run.channels[2].event.waveform)
    
    self.tr_store["min_ch0"] = min(self.run.channels[0].event.waveform)
    self.tr_store["min_ch1"] = min(self.run.channels[1].event.waveform)
    self.tr_store["min_ch2"] = min(self.run.channels[2].event.waveform)



  def finalize(self):
    pass



# EOF
