Algorithms:
    PMT CALC VARS:
        The following algorithm calculates a several general variables that may be used in other algorithms
        as such where possible it will use the same names as in the var_algs.CalcVars() algorithm

    Calc Pulse Shape:
        DEPENDENT ON PMT CALC VARS
        The following algorithm calculates pulse shape variables for PMT pulses connected to scintillators
        it is currently a work in progress

    SPE (Single PhotoElectron) Charecterisation Algorithm:
        REQUIRES PMT CALC VARS TO BE RUN FIRST
        The following algorithm is used in single photoelectron characterisation measurements
        it checks for a laser signal in a suitable time window (with a sideband region also identified), 
        recalculates the pulse charge and and expresses the variables in a consistant manner for root histograms.
        Pulse charge is either fixed or floating but window must not overlap with baseline calculation

        Individual event is classified as (stored in self.tr_store["signal_type"])
            0 - non signal event
            1 - signal event
            2 - sidebar event

        The Algorithm has several optional arguments (default values are defined in pmt_constants.py)
            signal_region_start     - start location of the signal acceptance window (in samples)
            signal_region_width     - width of signal and sidebar region (insamples)
            sidebar_region_delay    - distance between end of signal region and start of sidebar region
            charge_window_width     - width of window used to determine SPE pulse charge
            prepeak_charge_window   - number of samples of charge window taken before the waveform peak if window is allowed to move
            charge_window_start     - gives start of charge window if fixed window location is used (-1 if not used, which is default)

    Emulate Trigger:
        DEPENDENT ON PMT CALC VARS - it is currently a work in progress
        The following algorithm simulates an offline trigger (which makes an amplitude based cut)
        which can be used to process data taken with a low threshold trigger.        
        The threshold used in the trigger emulated cut is either passed to the function as the variable 
        "trigger_threshold" or if no value is given it defaults to a value specified in pmt_constants.py

    Find Laser Peak:
        The following algorithm determines and generates the plot of laser signal peak location
        to define the trigger window. It does not require other algorithms.
        It only records events where the peak exceeds the laser threshold.

Variable List:
---------------------------------------------------------------------------------------------------------------------------------
    PMT CALC VARS:
        "event_count"           pr_store    Total number of recorded events in run
        "avg_event_rate"        pr_store    Average event rate (with no event selection)
        "run_start_time"        pr_store    Run start time (in digitiser clock time 8ns units)
        "run_end_time_corrected" pr_store   Run end time (in digitiser clock time 8ns units) corrected for clock overflow
        "clock_overflow_counter" pr_store   Counts number of clock overflows (clock overflow occurs every ~17s or 2^31 clock units)
        "previous_event_time"   pr_store    records time of previous event (used to check for clock overflow)
        "record_length"         tr_store    Event length in samples
        "trigger_ts"            tr_store    Trigger time stamp (in clock units)
        "event_nr"              tr_store    Event number (starts with 1, ie. event index +1)

        Channel Variables - [idx] is channel index, replace square brackets with index number ie. waveform[idx] -> waveform0
        "peak_height[idx]"      tr_store    Peak height (above baseline) expressed as a positive number
        "peak_location[idx]"    tr_store    Peak location (in samples)
        "baseline[idx]"         tr_store    Baseline calculated over 40 samples (currently hardcoded)
        "total_charge[idx]"     tr_store    Total waveform charge (for all samples)

    Calc Pulse Shape:

    SPE Characterisation Algorithm:
        "laser_ch_num"      pr_store    Channel number of laser channel
        "pmt_ch_num"        pr_store    Channel number of pmt channel
        "num_signal"        pr_store    Number of signal events
        "num_sidebar"       pr_store    Number of sidebar events
        "window_error"      pr_store    Flag for error in hardcoded window error (1 = error)
        "signal_type"       tr_store    event classification
        "signal_charge"     tr_store    charge of signal events (not used for non-signal events)
        "sidebar_charge"    tr_store    charge of sidebar events (not used for non-sidebar events)

    Emulated Trigger:
        "corrected_event_count" pr_store    Number of events that pass the threshold cut
        "common_event_trigger"  tr_store    Flag for if event passes trigger logic
        
        Channel Variables - [idx] is channel index, replace square brackets with index number ie. waveform[idx] -> waveform0
        "offline_trigger[idx]"  tr_store    Flag for if channel [idx] passes the emulated trigger cut (1 = accepted event)


    Find Laser Peak:
        "laser_location_array"  pr_store    Array of laser location data used to produce histograms
        "laser_peak_height"     tr_store    Laser peak height in absolute adc. if = -1 then pulse did not exceed threshold
        "laser_peak_location"   tr_store    Laser peak location. if = -1 then pulse did not exceed threshold