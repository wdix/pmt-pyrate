from pyrate.classes import Algorithm
from utils import typecast
import numpy as np
import array
import statistics

description= """First algorithm written to understand pyrate W.Dix 21.8.19"""

class LoadWave(Algorithm):
    #This prints basic information about each event
    
    def __init__(self, name = "LoadWave"):
        self.name=name
        
    def initialize(self):
        if self.run.form == "wd":
            print("File is from WaveDump")
            self.pr_store["num_wave"] = 0
            
    def execute(self):
        if self.run.form == "wd":
            #print event number
            event_num = self.run.channels[0].event.idx_event
            print("Event Number : " + str(event_num))
        
            self.tr_store["waveform0"]      = self.run.channels[0].event.waveform
            #self.tr_store["waveform1"]     = self.run.channels[1].event.waveform
            #self.tr_store["waveform2"]     = self.run.channels[2].event.waveform


    def finalize(self):
        pass
    
class PrintFirstSample(Algorithm):
    #This gets waveforms from Transient Store and prints the first entry
    
    def __init__(self, name = "PrintFirstSample"):
        self.name=name
    
    def initialize(self):
        print("Initialized PrintFirstSample Alogrithm")
        
    def execute(self):
        if self.run.form == "wd":
            waveform0=self.tr_store["waveform0"]
            print waveform0
    
    def finalize(self):
        pass
