#   The following document only contains constants used in the algorithms in
#       pmt_alg.py. This document is not to contain any algorithms.

#   Author: W.Dix

# General Constants
#-----------------------------------------------------------------------------------------------------
NUM_BASELINE = 30
    # number of samples used in calculating the baseline

LASER_THRESHOLD = 5000
    # gives the absolute threshold used to determine the laser trigger

CHARGE_CONSTANT = 2.0/(2.0**14)/50 * (2e-9) * 1e12
    # conversion constant of charge from adc*samples to pC
    

# SPE Algorithm Constants
#-----------------------------------------------------------------------------------------------------
#   These constants are used in single photo electron algorithms
SIGNAL_REGION_START = 80
    # describes the default start location of the signal acceptance window in samples

SIDEBAR_REGION_DELAY = 20
    # describes the default distance between the end of the signal region and the start of the sidebar region
    #   ie the sidebar region starts at (SIGNAL_REGION_START+SIGNAL_REGION_WIDTH+SIDEBAR_REGION_DELAY)

SIGNAL_REGION_WIDTH = 40
    # describes the default width in samples of the acceptance window of the signal and sidebar regions

PREPEAK_CHARGE_WINDOW = 10
    # describes how many samples before the maximum peak that the charge integration window starts

CHARGE_WINDOW_WIDTH = 40
    # describes the width of the short charge integration window

CHARGE_WINDOW_START=-1
    # used to describe if a fixed charge window is used for all events, if <0 uses floating window, if >= 0 becomes start of window


# Dark Rate Constants
#-----------------------------------------------------------------------------------------------------
#   These constants are used in dark rate algorithm
DARK_RATE_THRESHOLD = 10
    # default threshold used for dark rate measurements


# Offline Trigger Constants
#-----------------------------------------------------------------------------------------------------
#   These constants are used in generating an offline trigger to refine rate calculations
NUM_ABOVE_THRESHOLD = 2
    # The number of samples above threshold required for a trigger

PREPEAK_TRIGGER_WINDOW = 10
POSTPEAK_TRIGGER_WINDOW = 20
    # Define the window around the signal peak used to generate a software trigger

DEFAULT_TRIGGER_THRESHOLD = 9.5
    # The default threshold above baseline used for the software trigger, will be over ridden by by supplied value


# ROOT Pulse Shape Vars
#--------------------------------------------------------------------------------------------------------
#   These constants are used in the root pulse shape algroithm
EXTENDEND_CHARGE_WINDOW_WIDTH = 80
    # Defines the width of the extended charge window

EXTENDEND_CHARGE_WINDOW_PREPEAK = 20
    # Defines the start of the extended charge window before the peak value

CHARGE_TAIL_WIDTH = 100
    # Defines the length of the charge tail region that starts at the end of the extended charge window


# ROOT Average Waveform
#---------------------------------------------------------------------------------------------------------
#   These constants are used in the root average waveform algorithm
AVG_BEFORE_PEAK = 100
    # Defines the number of samples before the peak if the average waveform is peak centred

AVG_AFTER_PEAK =300
    # Defines the number of samples after the peak if the average waveform is peak centred