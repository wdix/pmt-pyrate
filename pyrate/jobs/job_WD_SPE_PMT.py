#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

#from pyrate.algorithms import var_algs
from pyrate.algorithms import pmt_alg, ROOT_pmt_alg, ROOT_algs
from pyrate.histograms import hist_config
from pyrate.chains     import chain_config

"""
This is just a test file, intended for showing a basic configuration.
"""

# log level
# ---------
# Select the severity of logging info

log_lvl = logging.DEBUG

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = -1

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively
FORM     = "wd"
RECORD_LENGTH = 520

PMT_SN_List     = ["KQ0059", "KQ0137"]
Voltage_List    = ["1100", "1200", "1300", "1400", "1500", "1600", "1700", "1800"]
#["1100", "1200", "1300", "1350", "1400","1450", "1500", "1550", "1600", "1650", "1700", "1750", "1800"]

INPATH  = "/home/wdix/Data/R5912_OilProof/SPE"

OUTPATH = INPATH
OUTPATH_hist = os.path.join(OUTPATH, "histograms")
OUTPATH_root = os.path.join(OUTPATH, "root_files")
OUTFILE_root = os.path.join(OUTPATH,"root_files/SPE_output.root")

#Declare list of channels
ch_list_laser   = [[0 for v in range(len(Voltage_List))] for p in range(len(PMT_SN_List))]
ch_list_pmt     = [[0 for v in range(len(Voltage_List))] for p in range(len(PMT_SN_List))]

#PMT Loop
for pmt_idx in range(len(PMT_SN_List)):
    PMT_SN = PMT_SN_List[pmt_idx]

    # Voltage Loop
    for v_idx in range(len(Voltage_List)):
        voltage = Voltage_List[v_idx]

        filename_str_l = PMT_SN + "-" + voltage + "V-SPE-Laser.txt" #PMT_SN + "_SPE_" + voltage + "V_Laser.txt"
        filename_str_p = PMT_SN + "-" + voltage + "V-SPE-PMT.txt"   #PMT_SN + "_SPE_" + voltage + "V_PMT.txt"

        #Declare Channels
        ch_list_laser[pmt_idx][v_idx] = Run(
            name = "Channel_" + PMT_SN + "_" + voltage + "V_Laser",
            form=FORM,
            infile = os.path.join(INPATH, filename_str_l),
            min_idx_event = EMIN,
            max_idx_event = EMAX,
            record_length = RECORD_LENGTH,
            source_type = "LASER",
            plot_scale_factor = 2.0
        )
        
        ch_list_pmt[pmt_idx][v_idx] = Run(
            name = "Channel_" + PMT_SN + "_" + voltage + "V_PMT",
            form=FORM,
            infile = os.path.join(INPATH, filename_str_p),
            min_idx_event = EMIN,
            max_idx_event = EMAX,
            record_length = RECORD_LENGTH,
            source_type = "PMT",
            plot_scale_factor = 2.0
        )


# List of runs:
# -------------
# This is the list of runs considered for the job. They should each correspond to a different input file.
# An option can be passed whether to read the run info for WaveDump acquisitions.

run_list = []

# PMT Loop
for pmt_idx in range(len(PMT_SN_List)):
    PMT_SN = PMT_SN_List[pmt_idx]

    # Voltage Loop
    for v_idx in range(len(Voltage_List)):
        voltage = Voltage_List[v_idx]

        # Add run to run list
        run_list.append(
            Run(
                name = PMT_SN + "_" + voltage + "V-SPE_Response",
                form=FORM,
                read_run_info = False,
                channels = [
                    ch_list_laser[pmt_idx][v_idx],
                    ch_list_pmt[pmt_idx][v_idx]
                ]
            )
        )


#list of chains:
#----------------
chains_list = []
chains_list.append(chain_config.out_chain_spe_psd)


# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

job = Job(name="job_SPE", run_list=run_list, log_lvl = log_lvl, chains_list=chains_list)
job += pmt_alg.CalcPMTVars()
job += ROOT_pmt_alg.ROOT_SPE_Characterisation(out_path = OUTPATH_hist)
job += ROOT_pmt_alg.ROOT_Pulse_Shape_Vars()
job += ROOT_pmt_alg.ROOT_Fill_Chain(chain_type = "pmt_spe_psd", output_chain="pmt_spe_psd", reload_chain = True)
job += ROOT_algs.WriteROOTFile(outfile = OUTFILE_root, tree_list = chains_list, opt="RECREATE")
#job += ROOT_pmt_alg.ROOT_SPE_ROC_writeout(out_path=INPATH)

# EOF
