#!/dusr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging
from pyrate.jobs.job_WD_SPE_PMT import PMT_SN_List

from pyrate.classes  import Run
from pyrate.classes  import Job

#from pyrate.algorithms import var_algs
from pyrate.algorithms import pmt_alg, ROOT_pmt_alg, ROOT_algs
from pyrate.histograms import hist_config
from pyrate.chains     import chain_config


# log level
# ---------
# Select the severity of logging info

log_lvl = logging.INFO

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = -1

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively
FORM     = "wd"
RECORD_LENGTH = 520

PMT_SN_List     = ["KQ0059", "KQ0137"]
Voltage_List    = ["1100", "1200", "1300", "1400", "1500", "1600", "1700", "1800"]  # ["1200", "1300", "1400", "1500", "1600", "1700"]
#Note temperature names are representative and not exact
Temperature_List = ["05", "10", "15", "17", "20","22", "25", "27", "30"]
# The following list identifies any missing files that the loop would expect but are missing.
#   The list is automatically filled with the missing file class when the channel creation loop runs
Missing_File_list = []
class missing_file_class:
    def __init__(self, PMT_SN, Voltage, Temperature):
        self.PMT_SN = PMT_SN
        self.Voltage = Voltage
        self.Temperature = Temperature


INPATH  = "/home/wdix/Data/DR_Temperature/R5912_data"

OUTPATH = INPATH
OUTPATH_hist = os.path.join(OUTPATH, "histograms")
OUTPATH_root = os.path.join(OUTPATH, "root_files")
OUTFILE_root = os.path.join(OUTPATH, "root_files/DR_output_.root")

# Declare 3D array of channels
ch_array = [[[0 for v in range(len(Voltage_List))] for t in range(len(Temperature_List))] for p in range(len(PMT_SN_List))]

# PMT Loop
for pmt_idx in range(len(PMT_SN_List)):
    PMT_SN = PMT_SN_List[pmt_idx]

    # Temperature Loop
    for temp_idx in range(len(Temperature_List)):
        temperature = Temperature_List[temp_idx]

        # Voltage Loop
        for v_idx in range(len(Voltage_List)):
            voltage = Voltage_List[v_idx]
            
            filename_str = PMT_SN + "-DR_"+ temperature +"C_" + voltage + "V.txt"


            # check if file is present, if so adds creates run object
            if os.path.isfile(os.path.join(INPATH, filename_str)):
                # Declare channel
                ch_array[pmt_idx][temp_idx][v_idx] =  Run(
                    name = "Channel_" + PMT_SN + "_"+ temperature + "C_" + voltage +"V",
                    form=FORM, 
                    infile = os.path.join(INPATH, filename_str),
                    min_idx_event = EMIN,
                    max_idx_event = EMAX,
                    record_length = RECORD_LENGTH,
                    source_type = "PMT",
                    plot_scale_factor = 2.0
                )

            #if it does not exist print this out and add "NULL" to channel array
            else:
                ch_array[pmt_idx][temp_idx][v_idx] =  "NULL"
                print()
                print("File " + PMT_SN + " "+ temperature + "C " + voltage +"V Does not exist, skipping it")
                print()



# List of runs:
# -------------
# This is the list of runs considered for the job. They should each correspond to a different input file.
# An option can be passed whether to read the run info for WaveDump acquisitions.

run_list = []

#PMT Loop
for pmt_idx in range(len(PMT_SN_List)):
    PMT_SN = PMT_SN_List[pmt_idx]

    # Temperature Loop
    for temp_idx in range(len(Temperature_List)):
        Temperature = Temperature_List[temp_idx]

        # Voltage Loop
        for v_idx in range(len(Voltage_List)):
            Voltage = Voltage_List[v_idx]

            # Check if file is in missing ie array value is "NULL"
            if ch_array[pmt_idx][temp_idx][v_idx] == "NULL":
                continue
            

            # Add run to run list
            run_list.append(
                Run(
                    name = str(PMT_SN) + "_" + Voltage + "V_" + Temperature + "C-Dark_Rate", 
                    form=FORM,
                    read_run_info = False,
                    channels = [ch_array[pmt_idx][temp_idx][v_idx] ]
                )
            )




#list of chains:
#----------------
chains_list = []
chains_list.append(chain_config.out_chain_dr_psd)


# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

job = Job(name="job_DR", run_list=run_list, log_lvl = log_lvl, chains_list=chains_list)
job += pmt_alg.CalcPMTVars()
job += pmt_alg.Dark_Rate(dark_rate_threshold=10, out_path = OUTPATH_hist)
job += ROOT_pmt_alg.ROOT_Dark_Rate_Histogram(out_path = OUTPATH_hist)
job += ROOT_pmt_alg.ROOT_Pulse_Shape_Vars()
job += ROOT_pmt_alg.ROOT_Fill_Chain(chain_type = "pmt_dr_psd", output_chain="pmt_dr_psd", reload_chain = True)
job += ROOT_algs.WriteROOTFile(outfile = OUTFILE_root, tree_list = chains_list, opt="RECREATE")
#job += ROOT_pmt_alg.ROOT_DR_ROC_writeout(out_path = INPATH)

# EOF
