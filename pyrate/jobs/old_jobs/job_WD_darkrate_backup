#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

#from pyrate.algorithms import var_algs
from pyrate.algorithms import pmt_alg, ROOT_pmt_alg, ROOT_algs
from pyrate.histograms import hist_config
from pyrate.chains     import chain_config


"""
This is just a test file, intended for showing a basic configuration.
"""

# log level
# ---------
# Select the severity of logging info

log_lvl = logging.DEBUG

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = -1

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively

FORM     = "wd"

RECORD_LENGTH = 520

INPATH   = "/home/wdix/Documents/PMT_Analysis/Dark_Rate/Function_HV/SR513"

OUTPATH = "/home/wdix/Documents/PMT_Analysis/Dark_Rate/Function_HV/SR513"
OUTPATH_hist = os.path.join(OUTPATH, "histograms")
OUTPATH_root = os.path.join(OUTPATH, "root_files")
OUTFILE_root = os.path.join(OUTPATH, "root_files/DR_output_.root")

PMT_SN = "SR513"

ch1100 = Run(name = "Channel_1100", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1100V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.0)
ch1200 = Run(name = "Channel_1200", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1200V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.0)
ch1300 = Run(name = "Channel_1300", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1300V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.0)
ch1350 = Run(name = "Channel_1350", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1350V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.0)
ch1400 = Run(name = "Channel_1400", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1400V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.0)
ch1450 = Run(name = "Channel_1450", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1450V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.0)
ch1500 = Run(name = "Channel_1500", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1500V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.0)
ch1550 = Run(name = "Channel_1550", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1550V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.5)
ch1600 = Run(name = "Channel_1600", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1600V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.5)
ch1650 = Run(name = "Channel_1650", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1650V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.5)
ch1700 = Run(name = "Channel_1700", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1700V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.5)
ch1750 = Run(name = "Channel_1750", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1750V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 2.0)
ch1800 = Run(name = "Channel_1800", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_DR_1800V_4-5-20.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 2.0)

# List of runs:
# -------------
# This is the list of runs considered for the job. They should each correspond to a different input file.
# An option can be passed whether to read the run info for WaveDump acquisitions.

run_list = []

#run_list.append( Run(name = "SR513 1000V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1000]))
run_list.append( Run(name = "SR513 1100V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1100]))
run_list.append( Run(name = "SR513 1200V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1200]))
run_list.append( Run(name = "SR513 1300V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1300]))
run_list.append( Run(name = "SR513 1350V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1350]))
run_list.append( Run(name = "SR513 1400V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1400]))
run_list.append( Run(name = "SR513 1450V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1450]))
run_list.append( Run(name = "SR513 1500V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1500]))
run_list.append( Run(name = "SR513 1550V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1550]))
run_list.append( Run(name = "SR513 1600V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1600]))
run_list.append( Run(name = "SR513 1650V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1650]))
run_list.append( Run(name = "SR513 1700V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1700]))
run_list.append( Run(name = "SR513 1750V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1750]))
run_list.append( Run(name = "SR513 1800V - Dark Rate", form=FORM, read_run_info = False, channels = [ch1800]))


#list of chains:
#----------------
chains_list = []
chains_list.append(chain_config.out_chain_dr_psd)


# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

job = Job(name="job_DR", run_list=run_list, log_lvl = log_lvl, chains_list=chains_list)
job += pmt_alg.CalcPMTVars()
job += pmt_alg.Dark_Rate(dark_rate_threshold=10, out_path = OUTPATH_hist)
job += ROOT_pmt_alg.ROOT_Dark_Rate_Histogram(out_path = OUTPATH_hist)
job += ROOT_pmt_alg.ROOT_Pulse_Shape_Vars()
job += ROOT_pmt_alg.ROOT_Fill_Chain(chain_type = "pmt_dr_psd", output_chain="pmt_dr_psd", reload_chain = True)
job += ROOT_algs.WriteROOTFile(outfile = OUTFILE_root, tree_list = chains_list, opt="RECREATE")
#job += ROOT_pmt_alg.ROOT_DR_ROC_writeout(out_path = INPATH)

# EOF

