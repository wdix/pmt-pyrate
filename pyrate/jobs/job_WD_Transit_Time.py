#!/dusr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

#from pyrate.algorithms import var_algs
from pyrate.algorithms import pmt_alg, ROOT_pmt_alg, ROOT_algs
from pyrate.histograms import hist_config
from pyrate.chains     import chain_config


# log level
# ---------
# Select the severity of logging info

log_lvl = logging.INFO

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = -1

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively

FORM     = "wd"
RECORD_LENGTH = 520

INPATH  = "~/Data/R5912_OilProof/Transit_Time"
PMT_SN  = "KQ0137"

OUTPATH = "~/Data/R5912_OilProof/Transit_Time"
OUTPATH_hist = os.path.join(OUTPATH, "histograms")
OUTPATH_root = os.path.join(OUTPATH, "root_files")
OUTFILE_root = os.path.join(OUTPATH, "root_files/TT_output_.root")

ch_trig = Run(name = "Trigger_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_TransitTime-TriggerPMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.0)
ch_pmt  = Run(name = "R5912_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_TransitTime-PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT", plot_scale_factor = 1.0)
ch_laser = Run(name = "Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_TransitTime-Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser", plot_scale_factor = 1.0)



# List of runs:
# -------------
# This is the list of runs considered for the job. They should each correspond to a different input file.
# An option can be passed whether to read the run info for WaveDump acquisitions.

run_list = []

run_list.append( Run(name = str(PMT_SN) + "_TransitTime", form=FORM, read_run_info = False, channels = [ch_trig, ch_pmt, ch_laser]))


#list of chains:
#----------------
chains_list = []
chains_list.append(chain_config.out_chain_dr_psd)


# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

job = Job(name="job_DR", run_list=run_list, log_lvl = log_lvl, chains_list=chains_list)
job += pmt_alg.CalcPMTVars()
job += pmt_alg.Dark_Rate(dark_rate_threshold=0, out_path = OUTPATH_hist)
job += ROOT_pmt_alg.ROOT_Dark_Rate_Histogram(out_path = OUTPATH_hist)
job += ROOT_pmt_alg.ROOT_Pulse_Shape_Vars()
job += ROOT_pmt_alg.ROOT_Fill_Chain(chain_type = "pmt_dr_psd", output_chain="pmt_dr_psd", reload_chain = True)
job += ROOT_algs.WriteROOTFile(outfile = OUTFILE_root, tree_list = chains_list, opt="RECREATE")
#job += ROOT_pmt_alg.ROOT_DR_ROC_writeout(out_path = INPATH)

# EOF
