#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

from pyrate.algorithms import var_algs, ROOT_algs
from pyrate.histograms import hist_config
from pyrate.chains     import chain_config

from utils import sorted_ls


# --------------------------------------------------------------------------------------------
# WARNING: This production has been launched excluding even the last valid event for some runs
# as the indexing has been found to be inconsistent for some files. For some it starts from 0
# for others from 1.
# --------------------------------------------------------------------------------------------

"""
This is just a test file, intended for showing a basic configuration.
"""

# log level
# ---------
# Select the severity of logging info

log_lvl = logging.DEBUG

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = 10#-1

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively

FORM          = "wd"
RECORD_LENGTH = 128
n_channels    = 3

OUTPATH  = os.path.join("/nfs/sabre/copello/pyrate/pyrate/myNotebooks/myOutput","WD")
INPATH  = "/nfs/sabre2/data/SABRESouth"

filenames = sorted_ls(INPATH)

OUTFILE  = os.path.join(OUTPATH,"waves.root")

run_dict = {}
run_list = []

# --------------------
# find available files
# --------------------

for fn in filenames:
  if not ".txt" in fn: continue

  ch_name, start, end = fn.split("_")
  run_name = "_{}_{}".format(start,end.strip(".txt"))
  
  if not run_name in run_dict: run_dict[run_name] = [ch_name]
  else:                        run_dict[run_name].append(ch_name)
  
  if len(run_dict[run_name]) == n_channels:

    ch_list  = [] 
    
    for ch in run_dict[run_name]:
      
      # ----------------------
      # append to channel list
      # ----------------------
      ch_list.append( Run( name          = ch_name, 
                           form          = FORM, 
                           infile        = os.path.join(INPATH,ch+run_name+".txt"), 
                           min_idx_event = EMIN, 
                           max_idx_event = EMAX, 
                           record_length = RECORD_LENGTH) )

    # -------------------
    # append to run list
    # -------------------
    run_list.append( Run(name = run_name, form=FORM, read_run_info = True, channels = ch_list))


chains_list = []
chains_list.append(chain_config.out_chain)
chains_list.append(chain_config.out_md_chain)


# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

job = Job(name="job_WD_OldRuns", run_list=run_list, chains_list=chains_list, log_lvl = log_lvl)

job += var_algs.CalcVars()
job += var_algs.BaselineSub()
job += var_algs.CoincidenceFinder()

job += ROOT_algs.FillROOTChain(output_chain = "muons", reload_chain = True)
job += ROOT_algs.FillROOTChain(output_chain = "run_metadata", reload_chain = True)
job += ROOT_algs.WriteROOTFile(outfile = OUTFILE, tree_list = chains_list, opt="RECREATE")

# EOF
