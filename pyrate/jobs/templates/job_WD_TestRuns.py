#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

from pyrate.algorithms import var_algs, ROOT_algs
from pyrate.histograms import hist_config
from pyrate.chains     import chain_config

from utils import sorted_ls


# --------------------------------------------------------------------------------------------
# WARNING: This production has been launched excluding even the last valid event for some runs
# as the indexing has been found to be inconsistent for some files. For some it starts from 0
# for others from 1.
# --------------------------------------------------------------------------------------------

"""
This is just a test file, intended for showing a basic configuration.
"""

# log level
# ---------
# Select the severity of logging info

log_lvl = logging.DEBUG

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = -1

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively

FORM          = "wd"
RECORD_LENGTH = 128
n_channels    = 3

#OUTPATH  = "/home/cpyke/ForFrancesco"
OUTPATH  = "/home/cpyke/test_output"
OUTFILE  = os.path.join(OUTPATH,"out_test.root")

INPATH   = "/home/cpyke/Documents/DAQ/sabre_muons_daq/raw/18_12_21_17days_19_01_07"

#filenames = sorted_ls(INPATH)

# testing files
# -------------
filenames = []
filenames.append("wave0_s2018-12-24-18-49-19_e2018-12-24-19-49-19.txt")     # header of last event is broken in the middle of the first line
filenames.append("wave1_s2018-12-24-18-49-19_e2018-12-24-19-49-19.txt")     # last wf is broken 
filenames.append("wave2_s2018-12-24-18-49-19_e2018-12-24-19-49-19.txt")     # last header is broken. Lines missing.

#filenames.append("wave0_s2018-12-30-01-53-45_e2018-12-30-02-53-46.txt")
#filenames.append("wave1_s2018-12-30-01-53-45_e2018-12-30-02-53-46.txt")
#filenames.append("wave2_s2018-12-30-01-53-45_e2018-12-30-02-53-46.txt")

#filenames.append("waveTest_s2018-12-24-18-49-19_e2018-12-24-19-49-19.txt")  # last wf is broken
#filenames.append("waveTest2_s2018-12-24-18-49-19_e2018-12-24-19-49-19.txt") # last event is fully valid

#filenames.append("wave0_s2018-12-24-12-49-07_e2018-12-24-13-49-07.txt")
#filenames.append("wave1_s2018-12-24-12-49-07_e2018-12-24-13-49-07.txt")      # second header line of last event is incomplete
#filenames.append("wave2_s2018-12-24-12-49-07_e2018-12-24-13-49-07.txt")

#filenames.append("wave0_s2019-01-07-09-00-43_e2019-01-07-10-00-43.txt")

run_list = []
ch_list  = [] 

start_tmp, end_tmp = "",""

for fn in filenames:
    
  if not ".txt" in fn: continue

  ch_name, start, end = fn.split("_")

  if not start_tmp or end_tmp: start_tmp, end_tmp = start, end
  if not start_tmp == start and end_tmp == end: continue 
  
  if len(ch_list) < n_channels:
    ch_list.append( Run( name          = ch_name, 
                         form          = FORM, 
                         infile        = os.path.join(INPATH,fn), 
                         min_idx_event = EMIN, 
                         max_idx_event = EMAX, 
                         record_length = RECORD_LENGTH) )
  
  if len(ch_list) == n_channels:
    run_name = "run_WD_{}_{}".format(start,end.strip(".txt"))
    run_list.append( Run(name = run_name, form=FORM, read_run_info = True, channels = ch_list))
    ch_list = []


# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

job = Job(name="job_WD_OldRuns", run_list=run_list, log_lvl = log_lvl)
job += var_algs.CalcVars()
job += var_algs.CoincidenceFinder()
job += ROOT_algs.FillROOTChain(output_chain = chain_config.out_chain)
job += ROOT_algs.FillROOTChain(output_chain = chain_config.out_md_chain)
job += ROOT_algs.WriteROOTFile(outfile = OUTFILE, tree_list = [chain_config.out_chain, chain_config.out_md_chain], opt="RECREATE")

# EOF
