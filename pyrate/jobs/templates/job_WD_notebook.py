#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pyrate, ROOT


# In[2]:


import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

from pyrate.algorithms import var_algs, ROOT_algs, demo_algs
from pyrate.histograms import hist_config
from pyrate.chains     import chain_config


# In[3]:


log_lvl = logging.DEBUG


# In[4]:


EMIN     = 0
EMAX     = -1

FORM          = "wd"
RECORD_LENGTH = 128

OUTPATH  = os.path.join("/Users/fscutti/pyrate/myNotebooks/myOutput","WD")
INPATH   = os.path.join("/Users/fscutti/pyrate/myNotebooks/myData","WD")

OUTFILE  = os.path.join(OUTPATH,"waves.root")


# In[5]:


run0 = Run( name = "wave0",
                     form          = FORM,
                     infile        = os.path.join(INPATH,"wave0_s2019-09-07-08-22-13_e2019-09-07-09-22-13.txt"),
                     min_idx_event = EMIN,
                     max_idx_event = EMAX,
                     record_length = RECORD_LENGTH) 


run1 = Run( name = "wave1",
                     form          = FORM,
                     infile        = os.path.join(INPATH,"wave1_s2019-09-07-08-22-13_e2019-09-07-09-22-13.txt"),
                     min_idx_event = EMIN,
                     max_idx_event = EMAX,
                     record_length = RECORD_LENGTH) 

run2 = Run( name = "wave2",
                     form          = FORM,
                     infile        = os.path.join(INPATH,"wave2_s2019-09-07-08-22-13_e2019-09-07-09-22-13.txt"),
                     min_idx_event = EMIN,
                     max_idx_event = EMAX,
                     record_length = RECORD_LENGTH) 


# In[19]:


# Plotting. Completely independent from the job, only depends on 
# pyrate interfaces with input files. Here a local plot is used with 
# matplotlib.

import matplotlib.pyplot as plt

run0.load_run()

event0 = run0.get_event(0)
event1 = run0.get_event(2)
event2 = run0.get_event(100)

#event.print_info()

plt.xlim(110, 130)

plt.plot(xrange(len(event0.waveform)), event0.waveform)
plt.plot(xrange(len(event1.waveform)), event1.waveform)
plt.plot(xrange(len(event2.waveform)), event2.waveform)

plt.legend(["waveform 0","waveform 1","waveform 2"])
#plt.show()


# In[7]:


ch_list = [run0, run1, run2]


# In[8]:


run_list = [ Run(name = "s2019-09-07-08-22-13_e2019-09-07-09-22-13", form=FORM, read_run_info = True, channels = ch_list) ]


# In[9]:


chains_list = []
chains_list.append(chain_config.out_chain)
chains_list.append(chain_config.out_md_chain)


# In[10]:


# External histograms to be filled by job algorithms

h_min_ch0 = ROOT.TH1F("min_ch0","h_min_ch0", 20000,10000,20000)
h_min_ch0.SetLineColor(ROOT.kRed)

h_min_ch1 = ROOT.TH1F("min_ch1","h_min_ch1", 20000,10000,20000)
h_min_ch1.SetLineColor(ROOT.kBlue)

h_min_ch2 = ROOT.TH1F("min_ch2","h_min_ch2", 20000,10000,20000)
h_min_ch2.SetLineColor(ROOT.kGreen)


# In[11]:


# Instantiating the job and loading algorithms

job = Job(name="job_WD_demo", run_list=run_list, chains_list=chains_list, log_lvl = log_lvl)
job += var_algs.CalcVars()
job += var_algs.CoincidenceFinder()


# In[12]:


job += demo_algs.CalcMin()
job += demo_algs.FillPlot(hist=h_min_ch0)
job += demo_algs.FillPlot(hist=h_min_ch1)
job += demo_algs.FillPlot(hist=h_min_ch2)

job += demo_algs.FillPlot(plt=plt)


# In[13]:


job += ROOT_algs.FillROOTChain(output_chain = "muons", reload_chain = True)
job += ROOT_algs.FillROOTChain(output_chain = "run_metadata", reload_chain = True)
job += ROOT_algs.WriteROOTFile(outfile = OUTFILE, tree_list = chains_list, opt="RECREATE")


# In[14]:


# Launching the job

#job.launch()


# In[18]:


# Plotting with ROOT

c_min = ROOT.TCanvas("c_min","c_min")
c_min.cd()

#h_min_ch0.Draw()
#h_min_ch1.Draw("same")
#h_min_ch2.Draw("same")

c_min.SetLogy()
#c_min.Draw()


# In[ ]:




