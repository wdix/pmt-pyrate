#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

from pyrate.algorithms import ROOT_algs, var_algs
from pyrate.histograms import hist_config
from pyrate.chains import chain_config

"""
This is just a test file, intended for showing a basic configuration.
"""


# log level
# ---------
# Select the severity of logging info

log_lvl = logging.DEBUG

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = -1

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively

FORM     = "wc"

# input/output files:
# -------------------
# Absolute paths of the input and output files

INPATH   = "/home/cpyke/test_files/WCfiles/WaveCatcher_Data_WDix/Run_PMT_100_Data_4_11_2019_Ascii"
INFILE   = os.path.join(INPATH,"Run_PMT_TEST_Data_4_11_2019_Ascii.dat")

OUTPATH  = "/home/cpyke/test_output"
OUTFILE  = os.path.join(OUTPATH,"outwc.root")

# List of histograms:
# -------------------
# Histograms to be included in the job should be appended to this list
# and configured in hist_config.

#"""
hists_list = []
#hists_list.append(hist_config.wmax_ch0_hist)
#hists_list.append(hist_config.unixtime_hist)
#hists_list.append(hist_config.tdc_corr_hist)
hists_list.append(hist_config.event_number_hist)

#"""

chains_list = []
chains_list.append(chain_config.out_chain)

# List of runs:
# -------------
# This is the list of runs considered for the job. They should each correspond to a different input file.

run_list = []
run_list.append( Run(form=FORM, infile=INFILE, min_idx_event=EMIN, max_idx_event=EMAX) )


# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

job = Job(name = "job_WC", run_list = run_list, hists_list = hists_list, chains_list = chains_list, log_lvl = log_lvl)
job += var_algs.CalcVars()
#job += var_algs.CoincidenceFinder()
job += ROOT_algs.FillROOTHists(hlist = hists_list)
job += ROOT_algs.FillROOTChain(output_chain = "muDAQ")
job += ROOT_algs.WriteROOTFile(outfile = OUTFILE, hist_list = hists_list, tree_list = chains_list, opt="RECREATE")

# EOF
