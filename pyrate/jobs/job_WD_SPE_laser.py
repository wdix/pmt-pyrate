#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

#from pyrate.algorithms import var_algs
from pyrate.algorithms import pmt_alg, ROOT_pmt_alg
from pyrate.histograms import hist_config

"""
This is just a test file, intended for showing a basic configuration.
"""

# log level
# ---------
# Select the severity of logging info

log_lvl = logging.DEBUG

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = 10000

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively

FORM     = "wd"

RECORD_LENGTH = 520

INPATH   = "/home/wdix/Documents/PMT_Analysis/SPE/SR511"
PMT_SN = "SR511"

#_l refers to laser, _p is PMT 
ch1000_l = Run(name = "Channel_1000_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1000V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = 260, source_type = "Laser")
ch1100_l = Run(name = "Channel_1100_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1100V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1200_l = Run(name = "Channel_1200_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1200V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1300_l = Run(name = "Channel_1300_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1300V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1350_l = Run(name = "Channel_1350_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1350V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1400_l = Run(name = "Channel_1400_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1400V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1450_l = Run(name = "Channel_1450_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1450V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1500_l = Run(name = "Channel_1500_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1500V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1550_l = Run(name = "Channel_1550_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1550V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1600_l = Run(name = "Channel_1600_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1600V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1650_l = Run(name = "Channel_1650_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1650V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1700_l = Run(name = "Channel_1700_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1700V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1750_l = Run(name = "Channel_1750_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1750V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")
ch1800_l = Run(name = "Channel_1800_Laser", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1800V_Laser.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "Laser")

ch1000_p = Run(name = "Channel_1000_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1000V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = 260, source_type = "PMT")
ch1100_p = Run(name = "Channel_1100_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1100V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1200_p = Run(name = "Channel_1200_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1200V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1300_p = Run(name = "Channel_1300_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1300V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1350_p = Run(name = "Channel_1350_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1350V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1400_p = Run(name = "Channel_1400_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1400V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1450_p = Run(name = "Channel_1450_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1450V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1500_p = Run(name = "Channel_1500_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1500V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1550_p = Run(name = "Channel_1550_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1550V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1600_p = Run(name = "Channel_1600_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1600V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1650_p = Run(name = "Channel_1650_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1650V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1700_p = Run(name = "Channel_1700_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1700V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1750_p = Run(name = "Channel_1750_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1750V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")
ch1800_p = Run(name = "Channel_1800_PMT", form=FORM, infile = os.path.join(INPATH, str(PMT_SN) + "_SPE_1800V_PMT.txt"),  min_idx_event = EMIN, max_idx_event = EMAX, record_length = RECORD_LENGTH, source_type = "PMT")


# List of runs:
# -------------
# This is the list of runs considered for the job. They should each correspond to a different input file.
# An option can be passed whether to read the run info for WaveDump acquisitions.

run_list = []

#run_list.append( Run(name = str(PMT_SN) + " 1000V - SPE Response", form=FORM, read_run_info = False, channels = [ch1000_l, ch1000_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1100V - SPE Response", form=FORM, read_run_info = False, channels = [ch1100_l, ch1100_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1200V - SPE Response", form=FORM, read_run_info = False, channels = [ch1200_l, ch1200_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1300V - SPE Response", form=FORM, read_run_info = False, channels = [ch1300_l, ch1300_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1350V - SPE Response", form=FORM, read_run_info = False, channels = [ch1350_l, ch1350_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1400V - SPE Response", form=FORM, read_run_info = False, channels = [ch1400_l, ch1400_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1450V - SPE Response", form=FORM, read_run_info = False, channels = [ch1450_l, ch1450_p]))
run_list.append( Run(name = str(PMT_SN) + " 1500V - SPE Response", form=FORM, read_run_info = False, channels = [ch1500_l, ch1500_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1550V - SPE Response", form=FORM, read_run_info = False, channels = [ch1550_l, ch1550_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1600V - SPE Response", form=FORM, read_run_info = False, channels = [ch1600_l, ch1600_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1650V - SPE Response", form=FORM, read_run_info = False, channels = [ch1650_l, ch1650_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1700V - SPE Response", form=FORM, read_run_info = False, channels = [ch1700_l, ch1700_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1750V - SPE Response", form=FORM, read_run_info = False, channels = [ch1750_l, ch1750_p]))
#run_list.append( Run(name = str(PMT_SN) + " 1800V - SPE Response", form=FORM, read_run_info = False, channels = [ch1800_l, ch1800_p]))


# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

job = Job(name="job_WD", run_list=run_list, log_lvl = log_lvl)
job += ROOT_pmt_alg.ROOT_Find_Laser_Peak(out_path = INPATH)

# EOF
