#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

#from pyrate.algorithms import var_algs
from pyrate.algorithms import pmt_alg, ROOT_pmt_alg, ROOT_algs
from pyrate.histograms import hist_config
from pyrate.chains     import chain_config

"""
This is just a test file, intended for showing a basic configuration.
"""

# log level
# ---------
# Select the severity of logging info

log_lvl = logging.DEBUG

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = -1

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively
FORM     = "wd"
RECORD_LENGTH = 520

PMT_SN_List     = ["BC0174", "BC0175"]
Voltage_List    = ["1500", "1750"]
Test_Type_List  = ["DC_on_RG174", "DC_on_RG58", "DC_off_MC", "DC_off_SC", "DY_6", "DY_7", "PreAMP_V_match"]

INPATH  = "/home/wdix/Data/R11065/Base_test/DarkRuns"

OUTPATH = INPATH
OUTPATH_hist = os.path.join(OUTPATH, "histograms")
OUTPATH_root = os.path.join(OUTPATH, "root_files")
OUTFILE_root = os.path.join(OUTPATH,"root_files/SPE_output.root")

#Declare list of channels
#ch_list_laser   = [[[0 for v in range(len(Voltage_List))] for t in range(len(Test_Type_List))] for p in range(len(PMT_SN_List))]
ch_list_pmt     = [[[0 for v in range(len(Voltage_List))] for t in range(len(Test_Type_List))]for p in range(len(PMT_SN_List))]


#PMT Loop
for pmt_idx in range(len(PMT_SN_List)):
    PMT_SN = PMT_SN_List[pmt_idx]

    # Test Loop
    for test_idx in range(len(Test_Type_List)):
        test_type = Test_Type_List[test_idx] 

        # Voltage Loop
        for v_idx in range(len(Voltage_List)):
            voltage = Voltage_List[v_idx]

            filename_str_p = PMT_SN + "-" + voltage + "V-" + test_type+ "-DR.txt"   #PMT_SN + "_SPE_" + voltage + "V_PMT.txt"
            
            #Check if file is present
            if os.path.isfile(os.path.join(INPATH, filename_str_p)):
                # Declare channel
                ch_list_pmt[pmt_idx][test_idx][v_idx] = Run(
                    name = "Channel_" + PMT_SN + "_" + voltage + "V_" + test_type + "_DR",
                    form=FORM,
                    infile = os.path.join(INPATH, filename_str_p),
                    min_idx_event = EMIN,
                    max_idx_event = EMAX,
                    record_length = RECORD_LENGTH,
                    source_type = "PMT",
                    plot_scale_factor = 2.0
                )

            #if it does not exist print this out and add "NULL" to channel array
            else:
                ch_list_pmt[pmt_idx][test_idx][v_idx] = "NULL"
                print()
                print("File " + PMT_SN + " "+ test_type + " " + voltage +"V Does not exist, skipping it")
                print()

# List of runs:
# -------------
# This is the list of runs considered for the job. They should each correspond to a different input file.
# An option can be passed whether to read the run info for WaveDump acquisitions.

run_list = []

# PMT Loop
for pmt_idx in range(len(PMT_SN_List)):
    PMT_SN = PMT_SN_List[pmt_idx]

    # Test Loop
    for test_idx in range(len(Test_Type_List)):
        test_type = Test_Type_List[test_idx] 

        # Voltage Loop
        for v_idx in range(len(Voltage_List)):
            voltage = Voltage_List[v_idx]

            # Check if file is in missing ie array value is "NULL"
            if ch_list_pmt[pmt_idx][test_idx][v_idx] == "NULL":
                continue

            # Add run to run list
            run_list.append(
                Run(
                    name = PMT_SN + "_" + voltage + "V_" + test_type+ "_DR_Response",
                    form=FORM,
                    read_run_info = False,
                    channels = [
                        ch_list_pmt[pmt_idx][test_idx][v_idx]
                    ]
                )
            )


#list of chains:
#----------------
chains_list = []
chains_list.append(chain_config.out_chain_dr_psd_radio)


# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

job = Job(name="job_DR", run_list=run_list, log_lvl = log_lvl, chains_list=chains_list)
job += pmt_alg.CalcPMTVars()
job += pmt_alg.Dark_Rate(dark_rate_threshold=10, out_path = OUTPATH_hist)
job += ROOT_pmt_alg.ROOT_Dark_Rate_Histogram(out_path = OUTPATH_hist)
job += ROOT_pmt_alg.ROOT_Pulse_Shape_Vars()
job += ROOT_pmt_alg.ROOT_Radio_measure()
job += ROOT_pmt_alg.ROOT_Fill_Chain(chain_type = "pmt_dr_psd_radio", output_chain="pmt_dr_psd_radio", reload_chain = True)
job += ROOT_algs.WriteROOTFile(outfile = OUTFILE_root, tree_list = chains_list, opt="RECREATE")

# EOF
