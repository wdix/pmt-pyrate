#!/usr/bin/env python
# -*- coding: utf-8 -*-

description="""
Python class wrapping the ROOT tree class.
"""

__author__     = "Federico Scutti"
__email__      = "federico.scutti@unimelb.edu.au"
__maintainer__ = "Federico Scutti"
__date__       = "April 2019"

from ROOT import TChain, TTree
import sys

class Chain(object):
  
  def __init__(self, 
               name, 
               branches      = dict(), 
               title         = None, 
               branches_read = set(), 
               branches_on   = set(), 
               branches_off  = set(), 
               **kwargs
               ):
    
    self.name              = name
    self.branches          = branches
    self.title             = title or name
    self.branches_on       = branches_on
    self.branches_off      = branches_off
    self.branches_read     = branches_read

    self._chain            = None
    self._cache            = dict()

    for k,w in kwargs.iteritems():
      setattr(self, k, w)


  def __getattr__(self, name):
    """
    This function is called if name is not a normal attribute, it is
    assumed to be the name of a branch in self.tree.  The branches are
    cached after being read the first time to increase performance by
    avoiding reading the tree.  clear_cache() is called at the end of every
    event in the Job event loop.
    """
    try:
        return self._cache[name]
    except KeyError:
        if not name in self.branches_on:
            raise AttributeError("The %s branch is not turned-on." % name)
        self.branches_read.add(name)
        val = getattr(self._chain, name)
        self._cache[name] = val
        return val


  def load_chain(self, infiles = []): 
    """
    Load the chain structure. Either create it or take it from a file list with full data. 
    """
    
    if self.branches:
      
      self._chain = TTree(self.name, self.title)
      
      for bname, bdict in self.branches.iteritems():
        
        if "type" in bdict: self._chain.Branch(bname, bdict["value"], bdict["type"])
        else:               self._chain.Branch(bname, bdict["value"])
    
    if infiles:
      self._chain = TChain(self.name, self.title)
      for f in infiles: self._chain.Add(f)
    
    if not self.branches_on: self.set_all_branches_on()
    
    if self.branches_on:  self.set_branches_on(self.branches_on)
    if self.branches_off: self.set_branches_off(self.branches_off)


  def get_entry(self, idx): 
    """
    OK this is a bit overkill.
    """
    self._chain.GetEntry(idx)


  def get_entries(self): 
    """
    This too.
    """
    return self._chain.GetEntries()


  def fill_branch(self, bname, value):
    """
    Fill branch values. The value argument can be a list. 
    In this case its elements are pushed back into the branch. 
    Need to clear it at the end of the event processing.
    """
      
    if not bname in self.branches: 
      sys.exit("ERROR: branch {} does not exist for chain {}.".format(bname, self.name))
    
    elif "type" in self.branches[bname]: 
      self.branches[bname]["value"][0] = value 

    elif type(value) is list:
      for v in value: self.branches[bname]["value"].push_back(v)

    else: 
      self.branches[bname]["value"].push_back(value)


  def clear_vectors(self):
    """
    Clear vector branches at the end of a loop.
    """
    
    if not self.branches:
      sys.exit("ERROR: branches are not loaded for chain {}.".format(self.name))
    
    for bname, bdict in self.branches.iteritems():
      if not "type" in bdict: bdict["value"].clear()


  def fill_chain(self, bname = None, value = None, clear_vectors = True, clear_cache = True):
    """
    "Manually" fill the tree and automatically clear the vectors.
    This can also be used to fill a specific branch, but the tree filling
    will always follow, so using fill_branch externally in a loop is more
    economical with only a call to fill_chain at the end of the event. 
    Eventual vectors are cleared by default, as well as the cache so calling
    branches with the proxy will not be possible after this is executed.
    """
    if bname and value: self.fill_branch(bname, value)
    self._chain.Fill()
    
    if clear_vectors: self.clear_vectors()
    if clear_cache:   self.clear_cache() 

  def set_all_branches_off(self):
    """
    Switch off all branches.
    """
    self._chain.SetBranchStatus("*", 0)
    self.branches_on = set()


  def set_all_branches_on(self):
    """
    Switch on all branches.
    """
    if not self.branches:
      self.branches = [b.GetName() for b in self._chain.GetListOfBranches()]
    
    self._chain.SetBranchStatus("*", 1)
    self.branches_on = set(self.branches)


  def set_branches_off(self, branches):
    """
    Swith off only branches contained in a list.
    """
    for bn in branches:
      self._chain.SetBranchStatus(bn, 0)
      self.branches_on.discard(bn)


  def set_branches_on(self, branches):
    """
    Swith on only branches contained in a list.
    """
    for bn in branches:
      self._chain.SetBranchStatus(bn, 1)
      self.branches_on.add(bn)


  def clear_cache(self):
    """
    Clears the internal proxy cache
    """
    self._cache.clear()

# EOF
