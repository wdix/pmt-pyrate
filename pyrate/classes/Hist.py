#!/usr/bin/env python
# -*- coding: utf-8 -*-

description="""
Python class wrapping the ROOT histogram.
"""

__author__     = "Federico Scutti"
__email__      = "federico.scutti@unimelb.edu.au"
__maintainer__ = "Federico Scutti"
__date__       = "April 2019"

from ROOT import TH1F
import sys
from utils import typecast

class Hist1D(object):
  
  def __init__(self, name, nbins, xmin, xmax, vname="", wname="", xtitle="", ytitle="", title="", **kwargs):
    
    self.name   = name
    self.nbins  = nbins
    self.xmin   = xmin
    self.xmax   = xmax
    self.vname  = vname
    self.wname  = wname
    self.xtitle = xtitle
    self.ytitle = ytitle
    self.title  = title
    self._hist  = None
    
    for k,w in kwargs.iteritems():
      setattr(self, k, w)
    
    if not self.title:
      self.title = name

  def load_hist(self):
    """
    Load the histogram.
    """

    self._hist = TH1F(self.name, self.title, self.nbins, self.xmin, self.xmax)
    self._hist.GetXaxis().SetTitle(self.xtitle)
    self._hist.GetYaxis().SetTitle(self.ytitle)


  def fill_from_store(self, store):
    """
    Fill the histogram using variables in the transient store.
    A weight can be considered for the event which should also
    be in the transient store. If vname is not provided nothing happens.
    """
    
    if not self.vname: return

    if self.vname and not self.vname in store:
      sys.exit("ERROR: variable {} not found when filling {}".format(self.vname, self.name))
    
    if self.wname and not self.wname in store:
      sys.exit("ERROR: weight {} not found when filling {}".format(self.wname, self.name))
   
    if type(store[self.vname]) == list:
      """
      Not the preferred way to fill an histogram from a list.
      """
      for v in store[self.vname]:
        if self.wname: 
          self._hist.Fill(typecast(v), typecast(store[self.wname]))
        else: 
          self._hist.Fill(typecast(v), 1)
    
    else:
      if self.wname: 
        self._hist.Fill(typecast(store[self.vname]), typecast(store[self.wname]))
      else: 
        self._hist.Fill(typecast(store[self.vname]), 1)


  def fill(self, var, weight=1):
    """
    "Manually" fill the histogram.
    """
    self._hist.Fill(typecast(var), typecast(weight))

# EOF
