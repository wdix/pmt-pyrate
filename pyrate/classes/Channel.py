#!/usr/bin/env python
# -*- coding: utf-8 -*-

description="""
Python class representing individual channels.
WARNING: this is just a legacy class: don't use!!!
"""

__author__     = "Federico Scutti"
__email__      = "federico.scutti@unimelb.edu.au"
__maintainer__ = "Federico Scutti"
__date__       = "April 2019"


from FileReader import FileReader
from Event import Event, RunInfo

class Channel(FileReader):
  
  def __init__(self, 
               infile,
               form,
               name, 
               read_run_info = False, 
               min_idx_event = 0, 
               max_idx_event = -1,
               **kwargs
              ):
    
    self.name          = name or infile
    self.form          = form 
    FileReader.__init__(self, infile, form, read_run_info, min_idx_event, max_idx_event, **kwargs)

  def load_data(self):
    """
    Load the main channel data structures, and initializes 
    the file "pointer".
    """
    self.event         = Event(self.form)
    self.run_info      = RunInfo(self.form)
    self.load_file() 
    
  def update_event(self):
    """
    Updates content of event class.
    """
    
    self.fill_event(self.event)
    self.event.validate() 
    
    return
  
  def update_run_info(self):
    """
    Updates content of run_info class.
    """
    
    self.fill_run_info(self.run_info)
    
    return


  def _get_last_event(self):
    """
    "Private" method for reading the last event in the file.
    """
    
    last_event = Event(self.form)
    self.fill_event(last_event, read_backward=True)
    last_event.validate()
    
    return last_event


  def get_n_events(self):
    """
    Get total number of events from file.
    """
    n_events = self.max_idx_event
    
    if n_events < 0:
      last_event = self._get_last_event()
      n_events = int(last_event.header["Event Number"])
      self.max_idx_event = n_events   
    n_events += 1 
    
    return n_events

# EOF
