#!/usr/bin/env python
# -*- coding: utf-8 -*-

description="""
Python class handling the entire job.
"""
__author__     = "Federico Scutti"
__email__      = "federico.scutti@unimelb.edu.au"
__maintainer__ = "Federico Scutti"
__date__       = "April 2019"

import sys
from pyrate.classes import Run
import time
import logging
from tqdm import tqdm
from colorama import Fore
from collections import defaultdict
from copy import copy, deepcopy

class Job(object):
  
  def __init__(self, 
               name      = "Job", 
               version   = "test", 
               run_list  = [], 
               is_loop   = True, 
               logger    = None, 
               log_lvl   = None, 
               msg_space = 30,
               **kwargs
               ):
    
    self.name      = name
    self.version   = version
    self.run_list  = run_list
    self.is_loop   = is_loop
    self._algs     = []
    self._tr_store = defaultdict(lambda: None)
    self._pr_store = defaultdict(lambda: None)
    self.logger    = logger
    self.msg_space = msg_space

    if not log_lvl: self.log_lvl = logging.INFO
    else:           self.log_lvl = log_lvl
   
    # ----------------------------
    # set additional key-word args
    # ----------------------------
    
    for k,w in kwargs.iteritems():
        setattr(self, k, w)
   

  def __iadd__(self, alg):
    """
    The user should use this operator to schedule Algorithms to the Job.
    """
    alg._parent = self # set a reference to this job
    self._algs.append(alg)
    
    return self


  def get_logger(self, logger_name = "logger", log_file = None, msg_level = logging.INFO):
    """
    Set up the logger. This will be distributed to the algorithms later on.
    """
    
    logger  = logging.getLogger(logger_name)
    formatter    = logging.Formatter("[%(asctime)s %(name)-16s %(levelname)-7s]  %(message)s")
    
    if log_file:
      fileHandler  = logging.FileHandler(log_file, mode='w')
      fileHandler.setFormatter(formatter)
      logger.addHandler(fileHandler)
    
    logger.setLevel(msg_level)
    
    return logger

  
  def load(self, run = None, hists_list = [], chains_list = []):
    """
    Loads the current run for the algorithms and the run data structures. 
    Also, loads output structures to be used by algorithms like histograms
    and chains (trees). These are deepcopies of the original instance so that
    the Job will effective hold their only working copy and be entirely 
    responsible for their dereferencing upon _pr_store clearing.
    """

    if run:
     run.load_run()
     for alg in self._algs: alg.run = run
    
    if hists_list:
      for h in hists_list:
        self._pr_store[h.name] = deepcopy(h)
        self._pr_store[h.name].load_hist()
    
    if chains_list:
      for c in chains_list:
        self._pr_store[c.name] = deepcopy(c)
        self._pr_store[c.name].load_chain()


  def setup(self):
    """
    This sets up the run list and the two stores, transient and persistent.
    Notice that we do not explicitly initialize these objects in the derived
    Algorithms constructors.
    """

    if not self.logger:
      """
      Always sets up a logger and later distribute it to the algorithms.
      """
      log_file = "{}.{}.{}.log".format(self.name, self.version, time.strftime("%Y-%m-%d-%Hh%M"))
      
      self.logger = self.get_logger(logger_name = __name__+"("+self.name+")", 
                                    log_file    = log_file,
                                    msg_level   = self.log_lvl
                                    )
    for alg in self._algs:
      alg.tr_store = self._tr_store
      alg.pr_store = self._pr_store
      alg.logger   = self.logger

    if "hists_list" in self.__dict__:  self.load(hists_list = self.hists_list)
    if "chains_list" in self.__dict__: self.load(chains_list = self.chains_list)


  def initialize(self):
    """
    This method is executed before the loop starts.
    """
    for alg in self._algs: alg.initialize()
 

  def execute(self):
    """
    This method is executed for every single event. 
    Afterwards the transient store will be cleared.
    """
    for alg in self._algs: alg.execute()
 

  def finalize(self):
    """
    This method is executed at the end of the loop. 
    Afterwards the persistent store will be cleared.
    """
    for alg in self._algs: alg.finalize()
 

  def launch(self):
    """
    Launch the job.
    """
    self._time = time.time()
    
    self.setup()
    self.logger.info("Setting up job configuration.") 
    
    bar_format_run      = "{l_bar}%s{bar}%s{r_bar}" % (Fore.GREEN, Fore.RESET)
    bar_format_event    = "{l_bar}%s{bar}%s{r_bar}" % (Fore.YELLOW, Fore.RESET)
    bar_format_channels = "{l_bar}%s{bar}%s{r_bar}" % (Fore.BLUE, Fore.RESET)

    slen = self.msg_space - len(self.name)

    if not self.is_loop:
        
        channels = []
        
        for ch in tqdm(self.run_list, desc="run loop ({}) {}> ".format(self.name,"-"*slen), bar_format=bar_format_channels):
          channels.append(ch)
        
        self.load(Run(name="run_global",channels=channels))
        self.logger.info("Loading global run.")
        
        self.logger.info("Initializing algorithms.")
        self.initialize()

        self.logger.info("Finalize algorithms.")
        self.finalize()

    else:

       for run in tqdm(self.run_list, desc="run loop ({}) {}> ".format(self.name, "-"*slen), bar_format=bar_format_run):     
         
         self.logger.info("Loading run {}.".format(run.name))
         self.load(run=run)

         if run.read_run_info: run.update_run_info()
         
         emin, emax = run.min_idx_event, run.max_idx_event + 1
         if emax == 0: emax = run.get_n_events() 

         self.logger.info("Validating run {}.".format(run.name))
         #run.validate()
         
         self.logger.info("Initializing algorithms.")
         self.initialize()
         
         for i in tqdm(xrange(emin,emax), desc="event loop ({}) {}> ".format(self.name,"-"*(slen-2)), bar_format=bar_format_event):
      
           run.update_event()
           self.logger.debug("Updated event idx {}".format(i))
           
           self.execute()
           self.logger.debug("Executing algs for event idx {}".format(i))
           
           self._tr_store.clear()
           self.logger.debug("Transient store cleared for event idx {}".format(i))
           
           run.clear_cache()
         
         self.logger.info("Finalize algorithms.")
         self.finalize()
         
         self.logger.info("Closing input files.")
         run.close_files()

       print

    self._pr_store.clear() 
    self.logger.info("Permanent store cleared.")
    self.logger.info("Total execution time: {} s.".format(time.time() - self._time))

    return

# EOF
