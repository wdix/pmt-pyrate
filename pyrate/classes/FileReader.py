#!/usr/bin/env python
# -*- coding: utf-8 -*-

description="""
Python class to read DAQ files. Puts file content on the event.
"""

__author__     = "Federico Scutti"
__email__      = "federico.scutti@unimelb.edu.au"
__maintainer__ = "Federico Scutti"
__date__       = "April 2019"

import os
import re
import sys
import itertools
import collections
from copy import copy

from file_read_backwards import FileReadBackwards
import ROOT
import Chain

from utils import is_mixed_list, is_empty_list, get_valid_subset, typecast, has_letters, has_numbers, find_between
from utils import XOR, NXOR, NAND


class FileReader(object):
  """This class is the main engine for file reading. Different instances of FileReader should
  handle different files. If the format if WaveCatcher, the Run class handles the reading.
  If the format is WaveDump, the Channels class does it.

  Args:
    infile (str): input file
    form   (str): input file format


  """
  
  def __init__(self, infile, form, read_run_info, min_idx_event=0, max_idx_event=-1,  **kwargs):
    
    self.infile        = infile
    self.form          = form
    self.read_run_info = read_run_info
    self.min_idx_event = min_idx_event
    self.max_idx_event = max_idx_event
    
    self._thisfile     = None
    self._file_list    = []
    
    for k,w in kwargs.iteritems():
        setattr(self, k, w)


  def get_dir_content(self, path):
    """Return the content of the directory specified in the path. Will return 
    a dictionary where keys are object names. Mostly will work for histograms.

    Args:
      path (str): path where histograms should be retrieved

    """
    obj_dict = dict()

    for _f in self._file_list: 
      this_f = ROOT.TFile.Open(_f)
      obj_dir = this_f.GetDirectory(path)
      
      if not obj_dir:
        sys.exit("ERROR: path {} does not exist in {}!".format(path, _f))

      nxt = ROOT.TIter(obj_dir.GetListOfKeys())
      obj = nxt()
      
      while obj!=None:
        obj_name = obj.GetName()
        
        if not obj_name in obj_dict: obj_dict[obj_name] = copy(obj_dir.Get(obj_name))
        else:                        obj_dict[obj_name].Add(copy(obj_dir.Get(obj_name)))
        
        obj = nxt()
    
    return obj_dict  


  def load_file(self):
    """
    Load the file.
    """

    exists = os.path.isfile(self.infile)
    
    if not "root" in self.form:
      # -------------------------------------------------------------------------
      # Load the .txt file. The "pointer" holds the position at the last reading.
      # -------------------------------------------------------------------------
      
      if exists:  self._thisfile = open(self.infile,"r")
      else:       sys.exit("ERROR: file {} does not exist!".format(self.infile))
      
      # -------------------------------------------------------------------------
      # Load the file backwards. Useful for getting number of events.
      # -------------------------------------------------------------------------
      
      self._thisfile_bwd = FileReadBackwards(self.infile,"ascii") 
    
    else:
      # -------------------------------------------------------------------------
      # Load root files. This option supports wildcarding to catch all files
      # within a directory starting with a prefix specified before *.
      # -------------------------------------------------------------------------
      
      if exists:    
        self._file_list = [self.infile]
      
      elif "*" in self.infile:
        path,prefix = os.path.dirname(self.infile)+"/",self.infile.split("*")[0]
        fprefix = prefix.replace(path,"")
        
        self._file_list = [path+"/"+f for f in os.listdir(path) if (os.path.isfile(os.path.join(path, f)) and fprefix in f)]
      
      if not self._file_list:
        sys.exit("ERROR: no pattern found for file {}!".format(self.infile))
     
      
      # -------------------------------------------------------------------------
      # Load the tree structure. 
      # -------------------------------------------------------------------------
      
      if "input_chain" in self.__dict__:
        self.input_chain.load_chain( infiles = self._file_list )
      

      # -------------------------------------------------------------------------
      # Load histograms. This is a path in the input root file.
      # -------------------------------------------------------------------------
      
      if "input_hists" in self.__dict__:
        self.input_hists = self.get_dir_content(self.input_hists)


  def _reset_ptr(self, read_backward=False):
    """
    Resets the reading pointer.
    """

    if read_backward:
          self._thisfile_bwd.close()
          self._thisfile_bwd = FileReadBackwards(self.infile,"ascii")
    else:
          self._thisfile.close()
          self._thisfile = open(self.infile,"r")

  
  def _get_lines_between(self, start=1, stop=1, step=1, read_backward=False, reset_ptr=False):
    """
    Get the file lines between [start,stop] as an iterator (in python lingo, a 'generator').
    Lines at start,stop are included. Then dump the lines into a standard (small!) list.
    Indexing is supposed to start from 1. If an offset should be considered it is better
    to reset the pointer to the file. For now this option is only strictly necessary when dealing
    with backward reading.
    """
    lines = []
    
    file_ptr = None

    if not read_backward: 
        #if reset_ptr:
        #  self._thisfile.close()
        #  self._thisfile = open(self.infile,"r")

        file_ptr = self._thisfile
    
    else:                 
        #if reset_ptr:
        #  self._thisfile_bwd.close()
        #  self._thisfile_bwd = FileReadBackwards(self.infile,"ascii")

        file_ptr = self._thisfile_bwd
    
    islice = itertools.islice(file_ptr, start-1, stop, step)

    for line in islice: lines.append(line.rstrip())

    return lines
  
  
  def check_EOF(self, last_obj, lines):
    """
    Check if the EOF has been reached. If the lines are empty this is the case.
    However, the user should know by the #events the correct dimension of the 
    file. So, an error message will be thrown in this case and the last event
    info will be printed.
    """
    
    if not lines:
      last_obj.print_info(input_file = self.infile) 
      sys.exit("ERROR: no new lines read from file. Possible EOF reached.")
  
  
  def fill_event(self, event, read_backward=False, reset_ptr_after_read=False, **kw):
    """
    Fill the Event instance from Channel or Run.
    """
    
    new_idx_event = max(self.min_idx_event, event.idx_event+1)
    
    # --------------------------------    
    # fill events from wave-dump files
    # --------------------------------
    
    if self.form == "wd":
      new_header      = {}
      new_waveform    = []
      new_broken_size = []

      offset = 0

      if event.broken_size>0:
        offset += event.broken_size
        if self.read_run_info: offset += len(self.run_info.info)

      start = event.size * (new_idx_event - event.idx_event) - event.size + 1 + offset
      stop  = start + event.size - 1
     
      give_offset = offset > 0

      """
      Pointer reset before reading if offset is taken into account.
      """
      if give_offset: self._reset_ptr(read_backward)

      file_lines = self._get_lines_between(start=start,stop=stop,step=1, read_backward=read_backward)

      if reset_ptr_after_read: self._reset_ptr(read_backward)

      self.check_EOF(event, file_lines) 
      
      if event.has_header: 
        new_header = event.header.fromkeys(event.header,None)
      
      """
      N.B. the header and the waveform have to be filled sequentially!
      If a line is empty fill_event might run an additional time.
      """
      
      has_read_header   = 0
      has_read_waveform = False
      
      # debugging purpuses
      #if read_backward: print file_lines

      #if file_lines[0] == "":  del file_lines[0]
      #if file_lines[-1] == "": del file_lines[-1]

      for f_line in file_lines:
        
        header_flag = (has_letters(f_line) and event.has_header)
        if header_flag and NXOR(header_flag, read_backward, has_read_waveform):

          if ":" in f_line:
            k,v = f_line.split(":")
            new_header[k] = v.replace(" ","")
          
          # header might be broken so we don't have to check that is fully valid
          has_read_header += 1
          
        elif XOR(read_backward, has_read_header):
          new_waveform.append(typecast(f_line,priority="int"))
          has_read_waveform = bool(len(new_waveform) == event.record_length)
          if has_read_waveform and not read_backward: break

        else: 
          break
      
      len_new_header = len(get_valid_subset(new_header.values()))
      len_new_waveform = len(new_waveform)
      
      size = len_new_header + len_new_waveform 
      
      if size == event.size:  new_broken_size = 0
      elif size < event.size: new_broken_size = size + has_read_header - len_new_header 
      
      header_is_broken = (is_mixed_list(new_header.values()))
      wf_is_broken = (len(new_waveform) != event.record_length)

      event.update(header           = new_header, 
                   waveform         = new_waveform, 
                   idx_event        = new_idx_event, 
                   wf_is_broken     = wf_is_broken, 
                   header_is_broken = header_is_broken, 
                   broken_size      = new_broken_size)
    
    # -----------------------------------    
    # fill events from wave-catcher files
    # -----------------------------------
    elif self.form == "wc":
      new_header    = event.header.fromkeys(event.header,None)
      
      n_channels = int(kw["run_info"].info["NB OF CHANNELS ACQUIRED"])
      new_cdict = collections.OrderedDict()
      
      for nc in xrange(n_channels):
        new_cdict["ch{}".format(nc)] = event.cinfo.fromkeys(event.cinfo,None)
  
      event_size = 2 + n_channels * 2 
      start = event_size * (new_idx_event - event.idx_event) - event_size + 1
      stop  = start + event_size - 1 

      file_lines = self._get_lines_between(start=start,stop=stop,step=1,read_backward=read_backward)
      self.check_EOF(event, file_lines) 

      if read_backward: 
        
        new_idx_event = -1
        file_lines.reverse() 

      for f_line in file_lines:
        
        """
        N.B. first read the header which is assumed to always exist
        """
        
        if not "CH" in f_line and "=" in f_line:
          
          for k in new_header:
            if k in f_line:
              
              vec = f_line[f_line.index(k) + len(k):].split(" ")
              
              for v in vec:
                if not v in ["=",""]:
                  new_header[k] = v
                  break
        
        elif "CH" in f_line:
          
          global ck
          ck = "ch"+f_line.split(" ")[2]
          
          for i in xrange(len(new_cdict[ck].keys())-1):

            value = None
            key = new_cdict[ck].keys()[i]
            next_key = new_cdict[ck].keys()[i+1]
            
            if not next_key == "waveform":
              value = re.search('{}(.*){}'.format(key,next_key), f_line).group(1)
              #value = find_between(key, f_line, next_key)
            
            else:
              value = re.search('{}(.*){}'.format(key,"==="), f_line).group(1)
              #value = find_between(key, f_line, "===")
            
            value = value.replace(":","").replace("  ","")
            
            if value.startswith(" "): value = value[1:]
            if value.endswith(" "):   value = value[:-1]
            
            new_cdict[ck][key] = value
            
        elif not "=" in f_line and ck:
          
          new_cdict[ck]["waveform"] = [typecast(i,priority="float") for i in f_line.split(" ")]
          
          ck = None 
        
      event.update(header=new_header, cdict=new_cdict, idx_event=new_idx_event)
    
    # ---------------------------    
    # fill events from root files
    # ---------------------------
    elif self.form == "root":
      event.update(idx_event=new_idx_event)



    # ----------------------------    
    # fill events from CREDO files
    # ----------------------------
    elif self.form == "cr":
   
      file_lines = self._get_lines_between(start=1,stop=1,step=1)
      readings = file_lines[0].split(" ")

      new_lts    = typecast(readings[0])         
      new_rts    = typecast(readings[1])     
      new_lbs    = typecast(readings[2])        
      new_rbs    = typecast(readings[3])      
      new_xcoord = typecast(readings[4])      
      new_ycoord = typecast(readings[5])
      
      event.update(lts        = new_lts,
                   rts        = new_rts,
                   lbs        = new_lbs,
                   rbs        = new_rbs,
                   xcoord     = new_xcoord, 
                   ycoord     = new_ycoord, 
                   idx_event  = new_idx_event)

    return

  def fill_run_info(self, run_info):
    """
    Fill the RunInfo instance from the Channel or Run.
    """
    
    # ----------------------------------    
    # fill run info from wave-dump files
    # ----------------------------------
    if self.form == "wd":
      file_lines = self._get_lines_between(start=1,stop=len(run_info.info),step=1,read_backward=True)
      self.check_EOF(run_info, file_lines) 
      
      run_info.info = run_info.info.fromkeys(run_info.info,None)
      
      for f_line in file_lines:
        if ":" in f_line:
          k = f_line.split(":")[0]
          v = f_line.split(":")[1]
          
          if "Trg Rate" in k: 
            k = "Trg Rate"
            v = f_line
          
          run_info.info[k] = v.replace(" ","")
    
    # -------------------------------------    
    # fill run info from wave-catcher files
    # -------------------------------------
    elif self.form == "wc":
      file_lines = self._get_lines_between(start=1,stop=4,step=1)
      self.check_EOF(run_info, file_lines) 
      
      run_info.info = run_info.info.fromkeys(run_info.info,None)
      
      for f_line in file_lines:
        for k in run_info.info:
          
          if k in f_line:
            v = f_line.split(k)[1].split("=")[0]
            for s in [":","[","]"]:
              if s in v: v = v.replace(s,"")
            if v.startswith(" "): v = v[1:]
            if v.endswith(" "):   v = v[:-1]
            run_info.info[k] = v
    
    # ------------------------------    
    # fill run info from CREDO files
    # ------------------------------
    elif self.form == "cr":
      file_lines = self._get_lines_between(start=1,stop=2,step=1)
      self.check_EOF(run_info, file_lines) 

      run_info.info = run_info.info.fromkeys(run_info.info,None)
 
      run_info.info["columns"]  = file_lines[0]
      run_info.info["readings"] = file_lines[1]
          
    else: pass

    return 


  def close_thisfile(self):
    """
    Close internal reference to files.
    """

    if not "root" in self.form:
      self._thisfile.close()
      self._thisfile_bwd.close()

    self._thisfile = None
    self._thisfile_bwd = None

# EOF
