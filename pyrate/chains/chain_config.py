#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pyrate.classes import Chain
from pyrate.chains import  branch_config as bc

"""
Database for chain configuration. If no branches are passed, it is assumed that the user
will read them off some input file. In this case, it will not be possible to modify the 
content of the branches.
"""

test_chain   = Chain(name = "nT")

# chains for old muon acquisition
# -------------------------------
out_chain    = Chain(name = "muons",        branches = bc.branches_dict)
out_md_chain = Chain(name = "run_metadata", branches = bc.branches_md_dict)

#PMT Chains
out_chain_pmt = Chain(name = "pmt_basic",       branches = bc.branches_dict_basic)
out_chain_spe = Chain(name = "pmt_spe",         branches = bc.branches_dict_spe)
out_chain_spe_psd = Chain(name = "pmt_spe_psd",     branches = bc.branches_dict_spe_psd)
out_chain_spe_psd_radio = Chain(name = "pmt_spe_psd_radio",     branches = bc.branches_dict_spe_psd_radio)
out_chain_dr  = Chain(name = "pmt_dr",          branches = bc.branches_dict_dr)
out_chain_dr_psd  = Chain(name = "pmt_dr_psd",  branches = bc.branches_dict_dr_psd)
out_chain_dr_psd_radio  = Chain(name = "pmt_dr_psd_radio",  branches = bc.branches_dict_dr_psd_radio)
# EOF
