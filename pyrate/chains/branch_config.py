#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import ROOT

description="""
This file is a database for branches holding their 
type and initialising their values. Branches are
pure python dictionaries up to this point.
"""

# -------------
# muon DAQ tree
# -------------
branches_dict = {}

branches_dict["record_length"]    = { "value" : np.zeros(1, dtype=int),  "type" : "record_length/s"    }   # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_dict["event_nr"]         = { "value" : np.zeros(1, dtype=long), "type" : "event_nr/i"         }   # C++ 'unsigned long' (32 bit), ROOT 'i' (UInt_t)
branches_dict["trigger_ts"]       = { "value" : np.zeros(1, dtype=long), "type" : "trigger_ts/i"       }   # C++ 'unsigned long' (32 bit), ROOT 'i' (UInt_t)
#branches_dict["bad_event"]        = { "value" : np.zeros(1, dtype=int),  "type" : "bad_event/b"        }   # C++ 'unsigned char' (8 bit), ROOT 'b' (UChar_t)
#branches_dict["incomplete_event"] = { "value" : np.zeros(1, dtype=int),  "type" : "incomplete_event/b" }   # C++ 'unsigned char' (8 bit), ROOT 'b' (UChar_t)
branches_dict["waveform0"]        = { "value" : ROOT.vector('double')()                                 }   # C++ 'short' (16 bit)
branches_dict["waveform1"]        = { "value" : ROOT.vector('double')()                                 }   # C++ 'short' (16 bit)
branches_dict["waveform2"]        = { "value" : ROOT.vector('double')()                                 }   # C++ 'short' (16 bit)
branches_dict["012_flag"]         = { "value" : np.zeros(1, dtype=int),  "type" : "flag_012/s"         }   # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_dict["12_flag"]          = { "value" : np.zeros(1, dtype=int),  "type" : "flag_12/s"          }   # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_dict["01_flag"]          = { "value" : np.zeros(1, dtype=int),  "type" : "flag_01/s"          }   # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_dict["02_flag"]          = { "value" : np.zeros(1, dtype=int),  "type" : "flag_02/s"          }   # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)


# ----------------------
# muon DAQ metadata tree
# ----------------------

branches_md_dict = {}

branches_md_dict["run_start"]         = { "value" : np.zeros(1, dtype=float), "type" : "run_start/D"        } # C++ 'double' (64 bit), ROOT 'D' (Double_t)
branches_md_dict["run_end"]           = { "value" : np.zeros(1, dtype=float), "type" : "run_end/D"          } # C++ 'double' (64 bit), ROOT 'D' (Double_t)
branches_md_dict["012_coin"]          = { "value" : np.zeros(1, dtype=int),   "type" : "coin_012/s"         } # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_md_dict["01_coin"]           = { "value" : np.zeros(1, dtype=int),   "type" : "coin_01/s"          } # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_md_dict["12_coin"]           = { "value" : np.zeros(1, dtype=int),   "type" : "coin_12/s"          } # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_md_dict["02_coin"]           = { "value" : np.zeros(1, dtype=int),   "type" : "coin_02/s"          } # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_md_dict["avg_trigger_rate"]  = { "value" : np.zeros(1, dtype=float), "type" : "avg_trigger_rate/D" } # C++ 'double' (64 bit), ROOT 'D' (Float_t)
branches_md_dict["avg_readout_rate"]  = { "value" : np.zeros(1, dtype=float), "type" : "avg_readout_rate/D" } # C++ 'double' (64 bit), ROOT 'D' (Float_t)
branches_md_dict["avg_efficiency_0"]  = { "value" : np.zeros(1, dtype=float), "type" : "avg_efficiency_0/D" } # C++ 'double' (64 bit), ROOT 'D' (Double_t)
branches_md_dict["avg_efficiency_1"]  = { "value" : np.zeros(1, dtype=float), "type" : "avg_efficiency_1/D" } # C++ 'double' (64 bit), ROOT 'D' (Double_t)
branches_md_dict["avg_efficiency_2"]  = { "value" : np.zeros(1, dtype=float), "type" : "avg_efficiency_2/D" } # C++ 'double' (64 bit), ROOT 'D' (Double_t)
branches_md_dict["trigger_rates"]     = { "value" : ROOT.vector('float')()                                  } # C++ 'float' (32 bit)
branches_md_dict["readout_rates"]     = { "value" : ROOT.vector('float')()                                  } # C++ 'float' (32 bit)


# ----------------------
# basic pmt tree
# ----------------------

branches_dict_basic = {}
# run data
branches_dict_basic["event_count"]      = { "value" : np.zeros(1, dtype=long),  "type" : "event_count/i"}
branches_dict_basic["run_time_corrected"]={ "value" : np.zeros(1, dtype=float), "type" : "run_time_corrected/D"}
# per event data
branches_dict_basic["event_nr"]         = { "value" : np.zeros(1, dtype=long),  "type" : "event_nr/i"}
branches_dict_basic["trigger_ts"]       = { "value" : np.zeros(1, dtype=long),  "type" : "trigger_ts/i"}
branches_dict_basic["peak_height0"]     = { "value" : np.zeros(1, dtype=float), "type" : "peak_height0/D"}
branches_dict_basic["peak_location0"]   = { "value" : np.zeros(1, dtype=int),   "type" : "peak_location/s"}
branches_dict_basic["baseline0"]        = { "value" : np.zeros(1, dtype=float), "type" : "baseline0/D"}
branches_dict_basic["total_charge0"]    = { "value" : np.zeros(1, dtype=float), "type" : "total_charge0/D"}
branches_dict_basic["corrected_trigger_time"]    = { "value" : np.zeros(1, dtype=float), "type" : "corrected_trigger_time/D"}




# ----------------------
# spe pmt tree
# ----------------------

branches_dict_spe = {}
# run data
branches_dict_spe["event_count"]        = { "value" : np.zeros(1, dtype=long),  "type" : "event_count/i"        }
branches_dict_spe["num_signal"]         = { "value" : np.zeros(1, dtype=long),  "type" : "num_signal/i"        }
branches_dict_spe["num_sidebar"]        = { "value" : np.zeros(1, dtype=long),  "type" : "num_sidebar/i"        }
branches_dict_spe["run_time_corrected"] = { "value" : np.zeros(1, dtype=float), "type" : "run_time_corrected/D" }
# per event data
branches_dict_spe["event_nr"]             = { "value" : np.zeros(1, dtype=long),  "type" : "event_nr/i"           }
branches_dict_spe["trigger_ts"]           = { "value" : np.zeros(1, dtype=long),  "type" : "trigger_ts/i"         }
branches_dict_spe["signal_type"]          = { "value" : np.zeros(1, dtype=int),  "type" : "signal_type/s"         }
branches_dict_spe["peak_height_pmt"]      = { "value" : np.zeros(1, dtype=float), "type" : "peak_height_pmt/D"    }
branches_dict_spe["peak_location_pmt"]    = { "value" : np.zeros(1, dtype=int),   "type" : "peak_location_pmt/s"      }
branches_dict_spe["baseline_pmt"]         = { "value" : np.zeros(1, dtype=float), "type" : "baseline_pmt/D"          }
branches_dict_spe["charge_pmt"]           = { "value" : np.zeros(1, dtype=float), "type" : "charge_pmt/D"      }
branches_dict_spe["peak_location_laser"]  = { "value" : np.zeros(1, dtype=int),   "type" : "peak_location_laser/s"      }
branches_dict_spe["peak_height_laser"]    = { "value" : np.zeros(1, dtype=float), "type" : "peak_height_laser/D"  }
branches_dict_spe["corrected_trigger_time"]    = { "value" : np.zeros(1, dtype=float), "type" : "corrected_trigger_time/D"}


# ----------------------
# spe_psd pmt tree
# ----------------------

branches_dict_spe_psd = {}
# run data
branches_dict_spe_psd["event_count"]        = { "value" : np.zeros(1, dtype=long),  "type" : "event_count/i"}
branches_dict_spe_psd["num_signal"]         = { "value" : np.zeros(1, dtype=long),  "type" : "num_signal/i"}
branches_dict_spe_psd["num_sidebar"]        = { "value" : np.zeros(1, dtype=long),  "type" : "num_sidebar/i"}
branches_dict_spe_psd["run_time_corrected"] = { "value" : np.zeros(1, dtype=float), "type" : "run_time_corrected/D"}
# per event data
branches_dict_spe_psd["event_nr"]           = { "value" : np.zeros(1, dtype=long),  "type" : "event_nr/i"}
branches_dict_spe_psd["trigger_ts"]         = { "value" : np.zeros(1, dtype=long),  "type" : "trigger_ts/i"}
branches_dict_spe_psd["signal_type"]        = { "value" : np.zeros(1, dtype=int),  "type" : "signal_type/s"}
branches_dict_spe_psd["peak_height_pmt"]    = { "value" : np.zeros(1, dtype=float), "type" : "peak_height_pmt/D"}
branches_dict_spe_psd["peak_location_pmt"]  = { "value" : np.zeros(1, dtype=int),   "type" : "peak_location_pmt/s"}
branches_dict_spe_psd["baseline_pmt"]       = { "value" : np.zeros(1, dtype=float), "type" : "baseline_pmt/D"}
branches_dict_spe_psd["charge_pmt"]         = { "value" : np.zeros(1, dtype=float), "type" : "charge_pmt/D"}
branches_dict_spe_psd["peak_location_laser"]= { "value" : np.zeros(1, dtype=int),   "type" : "peak_location_laser/s"}
branches_dict_spe_psd["peak_height_laser"]  = { "value" : np.zeros(1, dtype=float), "type" : "peak_height_laser/D"}
branches_dict_spe_psd["corrected_trigger_time"]    = { "value" : np.zeros(1, dtype=float), "type" : "corrected_trigger_time/D"}
#psd data
branches_dict_spe_psd["charge_extended"]    = { "value" : np.zeros(1, dtype=float), "type" : "charge_extended/D"}
branches_dict_spe_psd["charge_pre_peak"]    = { "value" : np.zeros(1, dtype=float), "type" : "charge_pre_peak/D"}
branches_dict_spe_psd["charge_post_peak"]   = { "value" : np.zeros(1, dtype=float), "type" : "charge_post_peak/D"}
branches_dict_spe_psd["charge_tail"]        = { "value" : np.zeros(1, dtype=float), "type" : "charge_tail/D"}
branches_dict_spe_psd["mean_time"]          = { "value" : np.zeros(1, dtype=float), "type" : "mean_time/D"}
branches_dict_spe_psd["moment_1"]           = { "value" : np.zeros(1, dtype=float), "type" : "moment_1/D"}
branches_dict_spe_psd["moment_2"]           = { "value" : np.zeros(1, dtype=float), "type" : "moment_2/D"}
branches_dict_spe_psd["moment_3"]           = { "value" : np.zeros(1, dtype=float), "type" : "moment_3/D"}
branches_dict_spe_psd["moment_4"]           = { "value" : np.zeros(1, dtype=float), "type" : "moment_4/D"}
branches_dict_spe_psd["samples_above_threshold"]    = { "value" : np.zeros(1, dtype=long),  "type" : "samples_above_threshold/i"}


# ----------------------
# spe_psd_radio pmt tree
# ----------------------

branches_dict_spe_psd_radio = {}
# run data
branches_dict_spe_psd_radio["event_count"]        = { "value" : np.zeros(1, dtype=long),  "type" : "event_count/i"}
branches_dict_spe_psd_radio["num_signal"]         = { "value" : np.zeros(1, dtype=long),  "type" : "num_signal/i"}
branches_dict_spe_psd_radio["num_sidebar"]        = { "value" : np.zeros(1, dtype=long),  "type" : "num_sidebar/i"}
branches_dict_spe_psd_radio["run_time_corrected"] = { "value" : np.zeros(1, dtype=float), "type" : "run_time_corrected/D"}
# per event data
branches_dict_spe_psd_radio["event_nr"]           = { "value" : np.zeros(1, dtype=long),  "type" : "event_nr/i"}
branches_dict_spe_psd_radio["trigger_ts"]         = { "value" : np.zeros(1, dtype=long),  "type" : "trigger_ts/i"}
branches_dict_spe_psd_radio["signal_type"]        = { "value" : np.zeros(1, dtype=int),  "type" : "signal_type/s"}
branches_dict_spe_psd_radio["peak_height_pmt"]    = { "value" : np.zeros(1, dtype=float), "type" : "peak_height_pmt/D"}
branches_dict_spe_psd_radio["peak_location_pmt"]  = { "value" : np.zeros(1, dtype=int),   "type" : "peak_location_pmt/s"}
branches_dict_spe_psd_radio["baseline_pmt"]       = { "value" : np.zeros(1, dtype=float), "type" : "baseline_pmt/D"}
branches_dict_spe_psd_radio["charge_pmt"]         = { "value" : np.zeros(1, dtype=float), "type" : "charge_pmt/D"}
branches_dict_spe_psd_radio["peak_location_laser"]= { "value" : np.zeros(1, dtype=int),   "type" : "peak_location_laser/s"}
branches_dict_spe_psd_radio["peak_height_laser"]  = { "value" : np.zeros(1, dtype=float), "type" : "peak_height_laser/D"}
branches_dict_spe_psd_radio["corrected_trigger_time"]    = { "value" : np.zeros(1, dtype=float), "type" : "corrected_trigger_time/D"}
#psd data
branches_dict_spe_psd_radio["charge_extended"]    = { "value" : np.zeros(1, dtype=float), "type" : "charge_extended/D"}
branches_dict_spe_psd_radio["charge_pre_peak"]    = { "value" : np.zeros(1, dtype=float), "type" : "charge_pre_peak/D"}
branches_dict_spe_psd_radio["charge_post_peak"]   = { "value" : np.zeros(1, dtype=float), "type" : "charge_post_peak/D"}
branches_dict_spe_psd_radio["charge_tail"]        = { "value" : np.zeros(1, dtype=float), "type" : "charge_tail/D"}
branches_dict_spe_psd_radio["mean_time"]          = { "value" : np.zeros(1, dtype=float), "type" : "mean_time/D"}
branches_dict_spe_psd_radio["moment_1"]           = { "value" : np.zeros(1, dtype=float), "type" : "moment_1/D"}
branches_dict_spe_psd_radio["moment_2"]           = { "value" : np.zeros(1, dtype=float), "type" : "moment_2/D"}
branches_dict_spe_psd_radio["moment_3"]           = { "value" : np.zeros(1, dtype=float), "type" : "moment_3/D"}
branches_dict_spe_psd_radio["moment_4"]           = { "value" : np.zeros(1, dtype=float), "type" : "moment_4/D"}
branches_dict_spe_psd_radio["samples_above_threshold"]    = { "value" : np.zeros(1, dtype=long),  "type" : "samples_above_threshold/i"}

# radio data
branches_dict_spe_psd_radio["peak_num"]     = { "value" : np.zeros(1, dtype=long), "type" : "peak_num/i"}
branches_dict_spe_psd_radio["peak_sum"]     = { "value" : np.zeros(1, dtype=float), "type" : "peak_sum/D"}
branches_dict_spe_psd_radio["peak_avg"]    = { "value" : np.zeros(1, dtype=float), "type" : "peak_avg/D"}
branches_dict_spe_psd_radio["freq_50_mavg_peak"] = {"value" : np.zeros(1, dtype=float), "type" : "freq_50_mavg_peak/D"}
branches_dict_spe_psd_radio["freq_200_mavg_peak"] = {"value" : np.zeros(1, dtype=float), "type" : "freq_200_mavg_peak/D"}
branches_dict_spe_psd_radio["freq_50_peak"] = {"value" : np.zeros(1, dtype=float), "type" : "freq_50_peak/D"}
branches_dict_spe_psd_radio["freq_200_peak"] = {"value" : np.zeros(1, dtype=float), "type" : "freq_200_peak/D"}


# ----------------------
# dr pmt tree
# ----------------------

branches_dict_dr = {}
# run data
branches_dict_dr["event_count"]         = { "value" : np.zeros(1, dtype=long),  "type" : "event_count/i"}
branches_dict_dr["run_time_corrected"]  = { "value" : np.zeros(1, dtype=float), "type" : "run_time_corrected/D"}
# per event data
branches_dict_dr["event_nr"]             = { "value" : np.zeros(1, dtype=long),  "type" : "event_nr/i"}
branches_dict_dr["trigger_ts"]           = { "value" : np.zeros(1, dtype=long),  "type" : "trigger_ts/i"}
branches_dict_dr["peak_height_pmt"]      = { "value" : np.zeros(1, dtype=float), "type" : "peak_height_pmt/D"}
branches_dict_dr["peak_location_pmt"]    = { "value" : np.zeros(1, dtype=int),   "type" : "peak_location_pmt/s"}
branches_dict_dr["baseline_pmt"]         = { "value" : np.zeros(1, dtype=float), "type" : "baseline_pmt/D"}
branches_dict_dr["charge_pmt"]           = { "value" : np.zeros(1, dtype=float), "type" : "charge_pmt/D"}
branches_dict_dr["corrected_trigger_time"]    = { "value" : np.zeros(1, dtype=float), "type" : "corrected_trigger_time/D"}


# ----------------------
# dr_psd pmt tree
# ----------------------

branches_dict_dr_psd = {}
# run data
branches_dict_dr_psd["event_count"]         = { "value" : np.zeros(1, dtype=long),  "type" : "event_count/i"}
branches_dict_dr_psd["run_time_corrected"]  = { "value" : np.zeros(1, dtype=float), "type" : "run_time_corrected/D"}
# per event data
branches_dict_dr_psd["event_nr"]             = { "value" : np.zeros(1, dtype=long),  "type" : "event_nr/i"}
branches_dict_dr_psd["trigger_ts"]           = { "value" : np.zeros(1, dtype=long),  "type" : "trigger_ts/L"}
branches_dict_dr_psd["peak_height_pmt"]      = { "value" : np.zeros(1, dtype=float), "type" : "peak_height_pmt/D"}
branches_dict_dr_psd["peak_location_pmt"]    = { "value" : np.zeros(1, dtype=int),   "type" : "peak_location_pmt/s"}
branches_dict_dr_psd["baseline_pmt"]         = { "value" : np.zeros(1, dtype=float), "type" : "baseline_pmt/D"}
branches_dict_dr_psd["charge_pmt"]           = { "value" : np.zeros(1, dtype=float), "type" : "charge_pmt/D"}
branches_dict_dr_psd["corrected_trigger_time"]    = { "value" : np.zeros(1, dtype=float), "type" : "corrected_trigger_time/D"}

#psd data
branches_dict_dr_psd["charge_extended"]     = { "value" : np.zeros(1, dtype=float), "type" : "charge_extended/D"}
branches_dict_dr_psd["charge_pre_peak"]     = { "value" : np.zeros(1, dtype=float), "type" : "charge_pre_peak/D"}
branches_dict_dr_psd["charge_post_peak"]    = { "value" : np.zeros(1, dtype=float), "type" : "charge_post_peak/D"}
branches_dict_dr_psd["charge_tail"]        = { "value" : np.zeros(1, dtype=float), "type" : "charge_tail/D"}
branches_dict_dr_psd["mean_time"]           = { "value" : np.zeros(1, dtype=float), "type" : "mean_time/D"}
branches_dict_dr_psd["moment_1"]            = { "value" : np.zeros(1, dtype=float), "type" : "moment_1/D"}
branches_dict_dr_psd["moment_2"]            = { "value" : np.zeros(1, dtype=float), "type" : "moment_2/D"}
branches_dict_dr_psd["moment_3"]            = { "value" : np.zeros(1, dtype=float), "type" : "moment_3/D"}
branches_dict_dr_psd["moment_4"]            = { "value" : np.zeros(1, dtype=float), "type" : "moment_4/D"}
branches_dict_dr_psd["samples_above_threshold"] = { "value" : np.zeros(1, dtype=long),  "type" : "samples_above_threshold/i"}


# ----------------------
# dr_psd-radio pmt tree
# ----------------------

branches_dict_dr_psd_radio = {}
# run data
branches_dict_dr_psd_radio["event_count"]         = { "value" : np.zeros(1, dtype=long),  "type" : "event_count/i"}
branches_dict_dr_psd_radio["run_time_corrected"]  = { "value" : np.zeros(1, dtype=float), "type" : "run_time_corrected/D"}
# per event data
branches_dict_dr_psd_radio["event_nr"]             = { "value" : np.zeros(1, dtype=long),  "type" : "event_nr/i"}
branches_dict_dr_psd_radio["trigger_ts"]           = { "value" : np.zeros(1, dtype=long),  "type" : "trigger_ts/L"}
branches_dict_dr_psd_radio["peak_height_pmt"]      = { "value" : np.zeros(1, dtype=float), "type" : "peak_height_pmt/D"}
branches_dict_dr_psd_radio["peak_location_pmt"]    = { "value" : np.zeros(1, dtype=int),   "type" : "peak_location_pmt/s"}
branches_dict_dr_psd_radio["baseline_pmt"]         = { "value" : np.zeros(1, dtype=float), "type" : "baseline_pmt/D"}
branches_dict_dr_psd_radio["charge_pmt"]           = { "value" : np.zeros(1, dtype=float), "type" : "charge_pmt/D"}
branches_dict_dr_psd_radio["corrected_trigger_time"]    = { "value" : np.zeros(1, dtype=float), "type" : "corrected_trigger_time/D"}

#psd data
branches_dict_dr_psd_radio["charge_extended"]     = { "value" : np.zeros(1, dtype=float), "type" : "charge_extended/D"}
branches_dict_dr_psd_radio["charge_pre_peak"]     = { "value" : np.zeros(1, dtype=float), "type" : "charge_pre_peak/D"}
branches_dict_dr_psd_radio["charge_post_peak"]    = { "value" : np.zeros(1, dtype=float), "type" : "charge_post_peak/D"}
branches_dict_dr_psd_radio["charge_tail"]        = { "value" : np.zeros(1, dtype=float), "type" : "charge_tail/D"}
branches_dict_dr_psd_radio["mean_time"]           = { "value" : np.zeros(1, dtype=float), "type" : "mean_time/D"}
branches_dict_dr_psd_radio["moment_1"]            = { "value" : np.zeros(1, dtype=float), "type" : "moment_1/D"}
branches_dict_dr_psd_radio["moment_2"]            = { "value" : np.zeros(1, dtype=float), "type" : "moment_2/D"}
branches_dict_dr_psd_radio["moment_3"]            = { "value" : np.zeros(1, dtype=float), "type" : "moment_3/D"}
branches_dict_dr_psd_radio["moment_4"]            = { "value" : np.zeros(1, dtype=float), "type" : "moment_4/D"}
branches_dict_dr_psd_radio["samples_above_threshold"] = { "value" : np.zeros(1, dtype=long),  "type" : "samples_above_threshold/i"}
# radio data
branches_dict_dr_psd_radio["peak_num"]     = { "value" : np.zeros(1, dtype=long), "type" : "peak_num/i"}
branches_dict_dr_psd_radio["peak_sum"]     = { "value" : np.zeros(1, dtype=float), "type" : "peak_sum/D"}
branches_dict_dr_psd_radio["peak_avg"]    = { "value" : np.zeros(1, dtype=float), "type" : "peak_avg/D"}

branches_dict_dr_psd_radio["freq_50_mavg_peak"] = {"value" : np.zeros(1, dtype=float), "type" : "freq_50_mavg_peak/D"}
branches_dict_dr_psd_radio["freq_200_mavg_peak"] = {"value" : np.zeros(1, dtype=float), "type" : "freq_200_mavg_peak/D"}
branches_dict_dr_psd_radio["freq_50_peak"] = {"value" : np.zeros(1, dtype=float), "type" : "freq_50_peak/D"}
branches_dict_dr_psd_radio["freq_200_peak"] = {"value" : np.zeros(1, dtype=float), "type" : "freq_200_peak/D"}

# EOF
