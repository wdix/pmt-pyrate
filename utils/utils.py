#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re

description="""
Module containing general utility functions.
"""

__author__     = "Federico Scutti"
__email__      = "federico.scutti@unimelb.edu.au"
__maintainer__ = "Federico Scutti"
__date__       = "April 2019"


def XOR(a,b):
  return bool(a)!=bool(b)


def NAND(a,b):
  return not (bool(a) and bool(b))


def NXOR(a,b,c):
  """
  Intendami chi puo che m'intend' io.
  """
  return NAND(XOR(a,b),XOR(b,c))


def is_mixed_list(l):
  """
  Returns True if at least one element in the list is valid (not None)
  but false if all of them are valid.
  """

  return any(l) and not all(l)


def is_empty_list(l):
  return all(v == None for v in l)


def get_valid_subset(l):
  return [e for e in l if e!=None]


def find_between(begin, s, end):
  """
  Returns substring in s between begin and end.
  """
  return re.search("{}(.*){}".format(begin,end),s).group(1)


def has_letters(s):
  """
  Check is string contains at least one letter.
  """
  for i in s:
    if i.isalpha(): return True

def has_numbers(s):
  """
  Check is string contains at least one number.
  """
  for i in s:
    if i.isdigit(): return True

def typecast(s, priority='float'):
  """
  Automatic typecast.
  """
  pr = None

  if priority=="float":  pr = (float, int, str)
  if priority=="int":    pr = (int, float, str)
  if priority=="string": pr = (str, float, int)

  for fn in pr:
     try:   return fn(s)
     except ValueError: pass
  return s


def sorted_ls(path):
  """
  Automatically sort file content of directory by date of creation.
  Returns the sorted list of files.
  """
  mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
  return list(sorted(os.listdir(path), key=mtime))

# EOF
